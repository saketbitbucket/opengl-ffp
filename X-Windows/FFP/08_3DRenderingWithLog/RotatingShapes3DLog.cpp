#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <memory.h> 

#include <X11/Xlib.h>
#include <X11/Xutil.h> 
#include <X11/XKBlib.h>
#include <X11/keysym.h> 

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//namespaces
using namespace std;

//global variable declarations
bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;


float gAngle = 0.0f;
FILE *gpFile;

//Draw Function
void Update()
{
	fprintf(gpFile, "Entered in Update.\n"); //Printing log
	gAngle += 1.0f;

	if (gAngle > 360)
	{
		gAngle = 0.0f;
	}
        fprintf(gpFile, "Exit update.\n"); //Printing log
}

void DrawPyramid()
{
	fprintf(gpFile, "Entered in DrawPyramid.\n"); //Printing log
	glBegin(GL_TRIANGLES);
	{
		////Front face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f); //Apex
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f); //Left front
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f); //Right front

									   //Left face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f); //Apex
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f); //Left back
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f); //Left front

										//Right face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f); //Apex
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f); //Right front
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f); //Right back

										//Back face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f); //Apex
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f); //Right back
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f); //Left back

	}glEnd();
	fprintf(gpFile, "Exit DrawPyramid.\n"); //Printing log
}

void DrawCube()
{
	fprintf(gpFile, "Entered in DrawCube.\n"); //Printing log
	glBegin(GL_QUADS);
	{
		//Front Face
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f); //top Left front
		glVertex3f(-1.0f, -1.0f, 1.0f); //bottom Left front
		glVertex3f(1.0f, -1.0f, 1.0f); //bottom Right front
		glVertex3f(1.0f, 1.0f, 1.0f); //top Right front

									  //Back Face -- make Z negative of front face
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);

		//Left Face
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f); //top Left back
		glVertex3f(-1.0f, -1.0f, -1.0f); //bottom Left back
		glVertex3f(-1.0f, -1.0f, 1.0f); //bottom Left front
		glVertex3f(-1.0f, 1.0f, 1.0f); //top Left front

									   //Right Face
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);

		//Top Face
		glColor3f(0.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f); //top Right front
		glVertex3f(1.0f, 1.0f, -1.0f); //top Right back
		glVertex3f(-1.0f, 1.0f, -1.0f); //top Left back
		glVertex3f(-1.0f, 1.0f, 1.0f); //top Left front

									   //Bottom Face
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

	}glEnd();
        fprintf(gpFile, "Entered in DrawCube.\n"); //Printing log
}

//entry-point function
int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void resize(int,int);
	void uninitialize();
	
	//variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	
	bool bDone=false;
	
	gpFile=fopen("RenderingLog.txt", "w"); //Open file
	if (gpFile==NULL)
	{
	printf("Log File Can Not Be Created. EXitting Now ...\n");
	exit(0);
	}
	else
	{
	fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

        fprintf(gpFile, "Entered in main.\n"); //Printing log

	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Message Loop
	XEvent event;
	KeySym keysym;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
				switch(keysym)
					{
						case XK_Escape:
							uninitialize();
							exit(0);
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress: 
					switch(event.xbutton.button)
					{
						case 1: 
						    break;
						case 2: 
						    break;
						case 3: 
						    break;
						default:
						    break;
					}
					break;
				case MotionNotify: 
					break;
				case ConfigureNotify: 
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose: 
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		
		}
                Update();
		display();
	}

        fprintf(gpFile, "Exit main.\n"); //Printing log
	return(0);
}

void CreateWindow(void)
{
        fprintf(gpFile, "Entered in CreateWindow.\n"); //Printing log
	//function prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	
	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
                GLX_DOUBLEBUFFER, True,
                GLX_DEPTH_SIZE, 24,
		None 
	}; 

	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable To Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
		
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay, 
					    RootWindow(gpDisplay, gpXVisualInfo->screen), 
					    gpXVisualInfo->visual,
 					    AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);

	winAttribs.event_mask= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask |
			       StructureNotifyMask;
	
	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	
	gWindow=XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay,gpXVisualInfo->screen),
			      0,
			      0,
			      giWindowWidth,
			      giWindowHeight,
			      0,
			      gpXVisualInfo->depth,
			      InputOutput,
			      gpXVisualInfo->visual,
			      styleMask,
			      &winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"First OpenGL Window");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
        fprintf(gpFile, "Exit create window.\n"); //Printing log
}

void ToggleFullscreen(void)
{
	fprintf(gpFile, "Entered fullsceeen.\n"); //Printing log
	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	//code
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
	           RootWindow(gpDisplay,gpXVisualInfo->screen),
	           False,
	           StructureNotifyMask,
	           &xev);
	fprintf(gpFile, "Exit fullsceeen.\n"); //Printing log
}

void initialize(void)
{
	fprintf(gpFile, "Entered initialize.\n"); //Printing log
	//function prototype
	void resize(int, int);
	
	//code
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	resize(giWindowWidth,giWindowHeight);
	fprintf(gpFile, "Exit initialize .\n"); //Printing log
}

void display(void)
{
	fprintf(gpFile, "Entered display.\n"); //Printing log
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Rendering Command
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

        fprintf(gpFile, "Set matrix to identity.\n"); //Printing log

	glTranslatef(-2.0f, 0.0f, -6.0f);
            
	fprintf(gpFile, "translating mv matrix.\n"); //Printing log

	glRotatef(gAngle, 0, 1, 0);
        
        fprintf(gpFile, "rotating mv matrix.\n"); //Printing log

	DrawPyramid();
        
        fprintf(gpFile, "Draw Pyramid.\n"); //Printing log

	glLoadIdentity();
	glTranslatef(2.0f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);
	glRotatef(gAngle, 1, 1, 1);
	DrawCube();

	glXSwapBuffers(gpDisplay,gWindow);
        fprintf(gpFile, "Exit display .\n"); //Printing log
}

void resize(int width,int height)
{
	 fprintf(gpFile, "Entered resize .\n"); //Printing log
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, ((GLfloat)width / (GLfloat)height), 0.1, 100.0);
	fprintf(gpFile, "Exit resize .\n"); //Printing log
}

void uninitialize(void)
{
	fprintf(gpFile, "Entered uninitialize .\n"); //Printing log
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;	
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

        if (gpFile) //close file
        {
	  fprintf(gpFile, "Exit uninitialize \n Log File Is Successfully Closed.\n");
	  fclose(gpFile);
	  gpFile = NULL;
        }

}

