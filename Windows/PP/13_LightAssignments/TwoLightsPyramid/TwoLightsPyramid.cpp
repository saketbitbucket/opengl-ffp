#include <windows.h>
#include <stdio.h> // for FILE I/O
#include <gl\glew.h> // for GLSL extensions IMPORTANT : This Line Should Be Before #include<gl\gl.h> And #include<gl\glu.h>
#include <gl/GL.h>
#include "vmath.h"


#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;


GLuint gVao_Pyramid;
GLuint gVbo_Position;
GLuint gVbo_Normal;
bool gbAnimate;
bool gbLight;

float gAngle;
void update(void)
{
	gAngle += 0.1;
	if (gAngle > 360.0)
		gAngle = 0.0;
}


GLuint modelmatrix, viewmatrix, projectionmatrix;

GLuint L_KeyPressed_uniform;

//GLuint La_uniform;
//GLuint Ld_uniform;
//GLuint Ls_uniform;
//GLuint light_position_uniform;

/////////////////////////////////////////////////////
GLuint La0_uniform;
GLuint Ld0_uniform;
GLuint Ls0_uniform;
GLuint light0_position_uniform;

GLuint La1_uniform;
GLuint Ld1_uniform;
GLuint Ls1_uniform;
GLuint light1_position_uniform;
//////////////////////////////////////////////////////

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

//GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
//GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };
//
//GLfloat material_ambient[] = { 0.0f,0.0f,0.0f,1.0f };
//GLfloat material_diffuse[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat material_specular[] = { 1.0f,1.0f,1.0f,1.0f };
//GLfloat material_shininess = 50.0f;


////////////////////////////////////////////////////////////////////
//LIGHT0
GLfloat light0_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat light0_diffuse[] = { 1.0f,0.0f, 0.0f, 0.0f };
GLfloat light0_specular[] = { 1.0f,0.0f, 0.0f, 0.0f };
GLfloat light0_position[] = { 2.0f, 1.0f, 1.0f };

//LIGHT1
GLfloat light1_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat light1_diffuse[] = { 0.0f,0.0f, 1.0f, 0.0f };
GLfloat light1_specular[] = { 0.0f,0.0f, 1.0f, 0.0f };
GLfloat light1_position[] = { -2.0f, 1.0f, 1.0f };

//Material
GLfloat material_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f ;

/////////////////////////////////////////////////////////////////////









// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	void initialize(void);
	void uninitialize(void);
	void display(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// registering class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szClassName,
		TEXT("PP Two Lights Pyramid"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize
	initialize();

	// message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			if (gbAnimate == true)
				update();


			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46: // for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		case 0x41: // for 'A' or 'a'
			if (bIsAKeyPressed == false)
			{
				gbAnimate = true;
				bIsAKeyPressed = true;
			}
			else
			{
				gbAnimate = false;
				bIsAKeyPressed = false;
			}
			break;

		case 0x4C: // for 'L' or 'l'
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

struct Point
{
	double X;
	double Y;
	double Z;
};
Point findNormalFromThreePoints(Point p1, Point p2, Point p3)
{
	vmath::dvec3 vector1 = { p1.X - p2.X, p1.Y - p2.Y,  p1.Z - p2.Z };
	vmath::dvec3 vector2 = { p3.X - p2.X, p3.Y - p2.Y,  p3.Z - p2.Z };
	vmath::dvec3 normalVector = cross(vector1,vector2);
	//normalize(normalVector);
	Point normal = {normalVector[0],normalVector[1],normalVector[2] };
	return normal;
}

void initialize(void)
{
	// function declarations
	void uninitialize(void);
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	// choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW Initialization Code For GLSL ( IMPORTANT : It Must Be Here. Means After Creating OpenGL Context But Before Using Any OpenGL Function )
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER ***
	// create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
	
		"uniform vec3 u_La0;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec4 u_light0_position;" \
	
		"uniform vec3 u_La1;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec4 u_light1_position;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \
		
		"vec3 light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);" \
		"vec3 light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);" \

		"float tn_dot_ld0 = max(dot(transformed_normals, light0_direction),0.0);" \
		"float tn_dot_ld1 = max(dot(transformed_normals, light1_direction),0.0);" \

		"vec3 ambient0 = u_La0 * u_Ka;" \
		"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
		"vec3 reflection_vector0 = reflect(light0_direction, transformed_normals);" \
		"vec3 viewer_vector0 = normalize(-eye_coordinates.xyz);" \
		"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0), u_material_shininess);" \
		
		"vec3 ambient1 = u_La1 * u_Ka;" \
		"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
		"vec3 reflection_vector1 = reflect(light1_direction, transformed_normals);" \
		"vec3 viewer_vector1 = normalize(-eye_coordinates.xyz);" \
		"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0), u_material_shininess);" \

		
		
		"phong_ads_color=ambient0 + diffuse0 + specular0+ambient1 + diffuse1 + specular1;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 phong_ads_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";
	
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	modelmatrix = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	viewmatrix = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projectionmatrix = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// L/l key is pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// ambient color intensity of light
	La0_uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	// diffuse color intensity of light
	Ld0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	// specular color intensity of light
	Ls0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
	// position of light
	light0_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");;


	// ambient color intensity of light
	La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	// diffuse color intensity of light
	Ld1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	// specular color intensity of light
	Ls1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	// position of light
	light1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");;



	// ambient reflective color intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");;

	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	//getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	//gNumVertices = getNumberOfSphereVertices();
	//gNumElements = getNumberOfSphereElements();









	const GLfloat PyramidVertices[] =
	{
		0.0f,1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,

		0.0f,1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		-1.0f, -1.0f, -1.0f,

		0.0f,1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,

		0.0f,1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		1.0f, -1.0f, 1.0f,
	};

	GLfloat PyramidNormal[36];
	
	fprintf(gpFile, "\n\nPYRAMID NORMALS\n");
	for (int i = 0; i < 4; i++)
	{
		Point p1 = { PyramidVertices[9 * i + 0],PyramidVertices[9 * i + 1],PyramidVertices[9 * i + 2] };
		Point p2 = { PyramidVertices[9 * i + 3],PyramidVertices[9 * i + 4],PyramidVertices[9 * i + 5] };
		Point p3 = { PyramidVertices[9 * i + 6],PyramidVertices[9 * i + 7],PyramidVertices[9 * i + 8] };
		Point n = findNormalFromThreePoints(p1,p3,p2);
		PyramidNormal[9 * i + 0] = n.X; PyramidNormal[9 * i + 1] = n.Y; PyramidNormal[9 * i + 2] = n.Z;
		PyramidNormal[9 * i + 3] = n.X; PyramidNormal[9 * i + 4] = n.Y; PyramidNormal[9 * i + 5] = n.Z;
		PyramidNormal[9 * i + 6] = n.X; PyramidNormal[9 * i + 7] = n.Y; PyramidNormal[9 * i + 8] = n.Z;
	}










	// vao
	glGenVertexArrays(1, &gVao_Pyramid);
	glBindVertexArray(gVao_Pyramid);

	// position vbo
	glGenBuffers(1, &gVbo_Position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(PyramidVertices), PyramidVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_Normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_Normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(PyramidNormal), PyramidNormal, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	/*glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);
*/
	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black

	// set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;

	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}


void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 1);

		// setting light's properties
		glUniform3fv(La0_uniform, 1, light0_ambient);
		glUniform3fv(Ld0_uniform, 1, light0_diffuse);
		glUniform3fv(Ls0_uniform, 1, light0_specular);
		glUniform4fv(light0_position_uniform, 1, light0_position);

		glUniform3fv(La1_uniform, 1, light1_ambient);
		glUniform3fv(Ld1_uniform, 1, light1_diffuse);
		glUniform3fv(Ls1_uniform, 1, light1_specular);
		glUniform4fv(light1_position_uniform, 1, light1_position);

		// setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
	}

	// OpenGL Drawing
	// set all matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	mat4 rotationMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -15.0f);
	rotationMatrix = rotate(0.0f, gAngle, 0.0f);
	modelMatrix = modelMatrix * rotationMatrix; // ORDER IS IMPORTANT



	glUniformMatrix4fv(modelmatrix, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(viewmatrix, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projectionmatrix, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao_Pyramid);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);


	//glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);


	//glBindVertexArray(gVao_Cube);
	glDrawArrays(GL_TRIANGLES, 0, 12);/*
	glDrawArrays(GL_TRIANGLES, 3, 3);
	glDrawArrays(GL_TRIANGLES, 6, 3);
	glDrawArrays(GL_TRIANGLES, 9, 3);*/




	// *** unbind vao ***
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}




//void display(void)
//{
//	//code
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
//
//	// start using OpenGL program object
//	glUseProgram(gShaderProgramObject);
//
//	if (gbLight == true)
//	{
//		// set 'u_lighting_enabled' uniform
//		glUniform1i(L_KeyPressed_uniform, 1);
//
//		// setting light's properties
//		glUniform3fv(La_uniform, 1, lightAmbient);
//		glUniform3fv(Ld_uniform, 1, lightDiffuse);
//		glUniform3fv(Ls_uniform, 1, lightSpecular);
//		glUniform4fv(light_position_uniform, 1, lightPosition);
//
//		// setting material's properties
//		glUniform3fv(Ka_uniform, 1, material_ambient);
//		glUniform3fv(Kd_uniform, 1, material_diffuse);
//		glUniform3fv(Ks_uniform, 1, material_specular);
//		glUniform1f(material_shininess_uniform, material_shininess);
//	}
//	else
//	{
//		// set 'u_lighting_enabled' uniform
//		glUniform1i(L_KeyPressed_uniform, 0);
//	}
//
//	// OpenGL Drawing
//	// set all matrices to identity
//	mat4 modelMatrix = mat4::identity();
//	mat4 viewMatrix = mat4::identity();
//
//	// apply z axis translation to go deep into the screen by -2.0,
//	// so that triangle with same fullscreen co-ordinates, but due to above translation will look small
//	modelMatrix = translate(0.0f, 0.0f, -2.0f);
//
//	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
//	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
//	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);
//
//	// *** bind vao ***
//	glBindVertexArray(gVao_sphere);
//
//	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
//	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);
//
//	// *** unbind vao ***
//	glBindVertexArray(0);
//
//	// stop using OpenGL program object
//	glUseProgram(0);
//
//	SwapBuffers(ghdc);
//}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = perspective(10.0f, (float)width / (float)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	// destroy vao
	if (gVao_Pyramid)
	{
		glDeleteVertexArrays(1, &gVao_Pyramid);
		gVao_Pyramid = 0;
	}

	// destroy position vbo
	if (gVbo_Position)
	{
		glDeleteBuffers(1, &gVbo_Position);
		gVbo_Position = 0;
	}

	// destroy normal vbo
	if (gVbo_Normal)
	{
		glDeleteBuffers(1, &gVbo_Normal);
		gVbo_Normal = 0;
	}

	// destroy element vbo
	/*if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}*/

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment  shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	//Deselect the rendering context
	wglMakeCurrent(NULL, NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
