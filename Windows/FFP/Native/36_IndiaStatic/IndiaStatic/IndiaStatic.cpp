#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH  800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")


//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable decleration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

//Variables 

float thickness = 0.0f;

//Main()
//Functions -- drawing object on origin
void DrawMulticolorRect()
{
	glBegin(GL_QUADS);
	{
		glColor3f(255.0f / 255.0f, 153.0f / 255.0f, 51.0f / 255.0f);
		glVertex3f(-0.1f, 0.1f, 0.0);
		glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(-0.1f, -0.1f, 0.0);
		glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
		glVertex3f(0.1f, -0.1f, 0.0);
		glColor3f(255.0f / 255.0f, 153.0f / 255.0f, 51.0f / 255.0f);
		glVertex3f(0.1f, 0.1f, 0.0);
	}glEnd();

}

void DrawSinglecolorRect()  //Color will be set outside the function
{
	glBegin(GL_QUADS);
	{
		glVertex3f(-0.1f, 0.1f, 0.0);
		glVertex3f(-0.1f, -0.1f, 0.0);
		glVertex3f(0.1f, -0.1f, 0.0);
		glVertex3f(0.1f, 0.1f, 0.0);
	}glEnd();

}

void DrawI()
{
	glPushMatrix(); //save the initial orientation of matrix
	glScalef(0.04, 8, 1);
	DrawMulticolorRect();
	glPopMatrix(); //Set back initial matrix
}

void DrawN()
{
	//Space of two vertical lines is 1/10 of the screen width (it is 2 i.e -1 to 1)

	glPushMatrix(); //Save the starting axis location

	glScalef(0.04, 8, 1);
	DrawMulticolorRect();

	glPopMatrix(); //go to starting location of axis

	glPushMatrix();//Save initial axis location

	glTranslatef(0.1f, 0, 0);
	glRotatef(7.0f, 0, 0, 1); //Angle of roatation came from trial n error can be calculated using vector angle
	glScalef(0.04, 8.1, 1);
	DrawMulticolorRect();

	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0, 0);
	glScalef(0.04, 8, 1);
	DrawMulticolorRect();

	glPopMatrix();
}

void DrawD()
{
	glPushMatrix();
	glScalef(0.04, 8, 1);
	DrawMulticolorRect(); // | -Drawing vertical line
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0, 0); // move 1/10 of the screen width
	glScalef(0.04, 8, 1);
	DrawMulticolorRect();  // | | -Drawing parallel vertical line
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.082f, 0.8f, 0); // move 1/10 of the screen width
	glScalef(1.20, 0.06, 1);
	glColor3f(255.0f / 255.0f, 153.0f / 255.0f, 51.0f / 255.0f);
	DrawSinglecolorRect(); // draw horizontal line
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.082f, -0.8f, 0); // move 1/10 of the screen width
	glScalef(1.20, 0.06, 1);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f);
	DrawSinglecolorRect();  // draw horizontal line
	glPopMatrix();

}

void DrawA()
{
	glPushMatrix();

	glRotatef(-7.0f, 0, 0, 1);
	glScalef(0.04, 8.1, 1);
	DrawMulticolorRect();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0, 0);
	glRotatef(7.0f, 0, 0, 1);
	glScalef(0.04, 8.1, 1);
	DrawMulticolorRect();
	glPopMatrix();

}


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//Variable decleration

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//Code
	//Initilizing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Register Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline using Native Windowing "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		600,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}

//Wnd Proc()

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lparam)
{
	//Function prototype
	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code 
	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

		/*case WM_PAINT:
		display();
		break;
		return(0);*/

		/*case WM_ERASEBKGND:
		return(0);*/

	case WM_SIZE:
		resize(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, imsg, wParam, lparam));
}
void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable decleration
	PIXELFORMATDESCRIPTOR pfd;

	int ipixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize
	pfd.nSize = sizeof(PPIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; //Changes for 3D window

	ghdc = GetDC(ghwnd);

	ipixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (ipixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, ipixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	resize(WIN_WIDTH, WIN_HEIGHT);

	gbFullScreen = false;
	ToggleFullScreen();
}

//Logic:
//Looking at the TEXT INDIA it is visible that horizontally screen is divided in 10 parts

void display(void)
{
	//code 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Rendering Command
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(-0.8, 0, 0);
	DrawI();

	glTranslatef(0.2, 0, 0); //move to width/10 towards +x : note Draw I doesn't change orientation of axis
	DrawN();

	glTranslatef(0.4, 0, 0); //Move by width/10 
	DrawD();

	glTranslatef(0.4, 0, 0);
	DrawI();

	glTranslatef(0.3, 0, 0);
	DrawA();

	//Tricolor line
	glTranslatef(0.1, 0, 0.0);

	glPushMatrix();
	glTranslatef(0.0, 0.018, 0.0);
	glScalef(0.98f, 0.03, 0);
	glColor3f(255.0f / 255.0f, 153.0f / 255.0f, 51.0f / 255.0f); //orange
	DrawSinglecolorRect();
	glPopMatrix();

	glPushMatrix();
	glScalef(0.99f, 0.035, 0);
	glColor3f(1.0f, 1.0f, 1.0f); //white
	DrawSinglecolorRect();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0, -0.020, 0.0);
	glScalef(1.025f, 0.05, 0);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f); //green
	DrawSinglecolorRect();
	glPopMatrix();

	//glFlush(); -- Commented single buffer api
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(0, ((GLfloat)width / (GLfloat)height), 0.1, 100.0);
}

void uninitialize()
{
	//uninitialize code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE |
			SWP_NOOWNERZORDER |
			SWP_NOZORDER |
			SWP_NOSIZE
			| SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);

	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
}