/**
 * Automatically generated file. DO NOT MODIFY
 */
package window.astromedicomp.com.window;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "window.astromedicomp.com.window";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = -1;
  public static final String VERSION_NAME = "";
}
