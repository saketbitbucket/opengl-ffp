#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

GLUquadric *quadric = NULL;

//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable decleration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;
bool gbEnableLignt = false;

GLfloat light_ambiant[] = { 0.0f, 0.0f,0.0f,1.0f };
GLfloat light_diffuse[] = { 1.0f, 1.0f,1.0f,1.0f };
GLfloat light_specular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[] = { 0.0f,0.0f,1.0f };

GLfloat RotateX = 0.0f;
GLfloat RotateY = 0.0f;
GLfloat RotateZ = 0.0f;

GLfloat gAngle = 0.0;

GLboolean gRotate = GL_FALSE;

void Update()
{
	gAngle += 0.1f;

	if (gAngle > 360)
	{
		gAngle = 0.0f;
	}
}


// ***** 1st sphere on 1st column, emerald *****
GLfloat MAmbient[24][4] = 
{
	{ 0.0215f, 0.1745f, 0.0215f, 1.0f },
	{ 0.135f, 0.2225f, 0.1575f, 1.0f },
	{0.05375f, 0.05f, 0.06625f, 1.0f },
	{ 0.25f, 0.20725f, 0.20725f,1.0f },
	{ 0.1745f ,0.01175f, 0.01175f, 1.0f },
	{ 0.1f, 0.18725f, 0.1745f, 1.0f },
	{ 0.329412f, 0.223529f, 0.027451f, 1.0f },
	{ 0.2125f, 0.1275f, 0.054f, 1.0f },
	{ 0.25f,0.25f,0.25f,1.0f },
	{ 0.19125f,0.0735f,0.0225f,1.0f },
	{ 0.24725f,0.1995f,0.0745f,1.0f },
	{ 0.19225f,0.19225f,0.19225f,1.0f },
	{ 0.0f,0.0f,0.0f,1.0f },
	{ 0.0f,0.1f,0.06f,1.0f },
	{0.0f, 0.0f, 0.0f, 1.0f},
	{ 0.0f,0.0f,0.0f,1.0f },
	{ 0.0f,0.0f,0.0f,1.0f },
	{ 0.0f,0.0f,0.0f,1.0f },
	{ 0.02f,0.02f,0.02f,1.0f },
	{ 0.0f,0.05f,0.05f,1.0f },
	{ 0.0,0.05f,0.0f,1.0f },
	{ 0.05f,0.0f,0.0f,1.0f },
	{ 0.05f,0.05f,0.05f,1.0f },
	{ 0.05f, 0.05f, 0.0f, 1.0f}
};


GLfloat MDiffuse[24][4] = 
{
	{ 0.07568f, 0.61424f, 0.07568f, 1.0f },
	{ 0.54f, 0.89f, 0.63f, 1.0f },
	{ 0.18275f, 0.17f, 0.22525f, 1.0f },
	{ 1.0f, 0.829f, 0.829f, 1.0f },
	{ 0.61424f, 0.04136f, 0.04136f, 1.0f },
	{ 0.396f, 0.74151f, 0.69102f, 1.0f },
	{ 0.780392f, 0.568627f, 0.113725f, 1.0f },
	{ 0.714f, 0.4284f, 0.18144f, 1.0f },
	{ 0.4f,0.4f,0.4f,1.0f },
	{ 0.7038f,0.27048f,0.0828f,1.0f },
	{ 0.75164f,0.60648f,0.22648f,1.0f },
	{ 0.50754f,0.50754f,0.50754f,1.0f },
	{ 0.01f,0.01f,0.01f,1.0f },
	{ 0.0f,0.50980392f,0.50980392f,1.0f },
	{ 0.1f,0.35f,0.1f,1.0f },
	{ 0.5f,0.0f,0.0f,1.0f },
	{ 0.55f,0.55f,0.55f,1.0f },
	{ 0.5f,0.5f,0.0f,1.0f },
	{ 0.01f,0.01f,0.01f,1.0f },
	{ 0.4f,0.5f,0.5f,1.0f },
	{ 0.4f,0.5f,0.4f,1.0f },
	{ 0.5f,0.4f,0.4f,1.0f },
	{ 0.5f,0.5f,0.5f,1.0f },
	{ 0.5f,0.5f,0.4f,1.0f }
};


GLfloat MSpecular[24][4] = 
{
	{ 0.633f, 0.727811f, 0.633f, 1.0f },
	{ 0.316228f, 0.316228f, 0.316228f, 1.0f },
	{ 0.332741f, 0.328634f, 0.46435f, 1.0f },
	{ 0.296648f, 0.296648f, 0.296648f, 1.0f },
	{ 0.727811f, 0.626959f, 0.626959f, 1.0f },
	{ 0.297254f, 0.30829f, 0.306678f, 1.0f },
	{ 0.992157f, 0.941176f, 0.807843f, 1.0f },
	{ 0.393548f,0.271906f, 0.166721f,1.0f },
	{ 0.774597f,0.774597f,0.774597f,1.0f },
	{ 0.256777f,0.137622f,0.086014f,1.0f },
	{ 0.628281f,0.555802f,0.366065f,1.0f },
	{ 0.508273f,0.508273f,0.508273f,1.0f },
	{ 0.50f,0.50f,0.50f,1.0f },
	{ 0.50196078f,0.50196078f,0.50196078f,1.0f },
	{ 0.45f,0.55f,0.45f,1.0f },
	{ 0.7f,0.6f,0.6f,1.0f },
	{ 0.70f,0.70f,0.70f,1.0f },
	{ 0.60f,0.60f,0.50f,1.0f },
	{ 0.4f,0.4f,0.4f,1.0f },
	{ 0.04f,0.7f,0.7f,1.0f },
	{ 0.04f,0.7f,0.04f,1.0f },
	{ 0.7f,0.04f,0.04f,1.0f },
	{ 0.7f,0.7f,0.7f,1.0f },
	{ 0.7f,0.7f,0.04f,1.0f },
};


GLfloat MShininess[24] = 
{ 
	0.6 * 128,
	0.1 * 128,
	0.3 * 128,
	0.088 * 128,
	0.6 * 128,
	0.1 * 128,
	0.21794872 * 128,
	0.2 * 128,
	0.6 * 128,
	0.1 * 128,
	0.4 * 128,
	0.4 * 128,
	0.25 * 128,
	0.25 * 128,
	0.25 * 128,
	0.25 * 128,
	0.25 * 128,
	0.25 * 128,
	0.078125 * 128,
	0.078125 * 128,
	0.078125 * 128,
	0.078125 * 128,
	0.078125 * 128,
	0.078125 * 128
};

//Main()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//Variable decleration

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//Code
	//Initilizing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Register Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline using Native Windowing : Solar System "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		600,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				Update();
				display();
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}

//Wnd Proc()

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lparam)
{
	//Function prototype
	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code 
	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

		/*case WM_PAINT:
		display();
		break;
		return(0);*/

		/*case WM_ERASEBKGND:
		return(0);*/

	case WM_SIZE:
		resize(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;

		case 0x4C:
			if (gbEnableLignt)
			{
				glDisable(GL_LIGHTING);
				gbEnableLignt = false;
			}
			else
			{
				glEnable(GL_LIGHTING);
				gbEnableLignt = true;
			}
			break;

		case 0x58:
			RotateX = 1.0f;
			RotateY = 0.0f;
			RotateZ = 0.0f;
			gAngle = 0.0;
			gRotate = GL_TRUE;
			break;

		case 0x59:
			RotateX = 0.0f;
			RotateY = 1.0f;
			RotateZ = 0.0f;
			gAngle = 0.0;
			gRotate = GL_TRUE;
			break;

		case 0x5A:
			RotateX = 0.0f;
			RotateY = 0.0f;
			RotateZ = 1.0f;
			gAngle = 0.0;
			gRotate = GL_TRUE;
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, imsg, wParam, lparam));
}
void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable decleration
	PIXELFORMATDESCRIPTOR pfd;

	int ipixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize
	pfd.nSize = sizeof(PPIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; //Changes for 3D window

	ghdc = GetDC(ghwnd);

	ipixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (ipixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, ipixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambiant);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);


	glEnable(GL_LIGHT0);

	resize(WIN_WIDTH, WIN_HEIGHT);
}

void display(void)
{
	//code 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Rendering Command
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (gRotate == GL_TRUE)
	{
		glPushMatrix();
		glRotatef(gAngle, RotateX, RotateY, RotateZ);
		glLightfv(GL_LIGHT0, GL_POSITION, light_position);
		glPopMatrix();
	}
	//view transformation
	glTranslatef(-8.0f, 17.0f, -40.0f);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	for (int i = 0; i < 4; i++)
	{
		glPushMatrix();
		glTranslatef(i*5.0f, 0.0f, 0.0f);
		
		for (int j = 0; j < 6; j++)
		{
			glTranslatef(0.0f, - 5.0f, 0.0f);

			//set matrial
			int spherenumber = (i * 4) + j;
			GLfloat tempAmbient[4];
			tempAmbient[0] = MAmbient[spherenumber][0];
			tempAmbient[1] = MAmbient[spherenumber][1];
			tempAmbient[2] = MAmbient[spherenumber][2];
			tempAmbient[3] = MAmbient[spherenumber][3];
			glMaterialfv(GL_FRONT, GL_AMBIENT, tempAmbient);

			GLfloat tempdiffuse[4];
			tempdiffuse[0] = MDiffuse[spherenumber][0];
			tempdiffuse[1] = MDiffuse[spherenumber][1];
			tempdiffuse[2] = MDiffuse[spherenumber][2];
			tempdiffuse[3] = MDiffuse[spherenumber][3];
			glMaterialfv(GL_FRONT, GL_DIFFUSE, tempdiffuse);


			GLfloat tempSpecular[4];
			tempSpecular[0] = MSpecular[spherenumber][0];
			tempSpecular[1] = MSpecular[spherenumber][1];
			tempSpecular[2] = MSpecular[spherenumber][2];
			tempSpecular[3] = MSpecular[spherenumber][3];
			glMaterialfv(GL_FRONT, GL_SPECULAR, tempSpecular);


			glMaterialf(GL_FRONT, GL_SHININESS, MShininess[spherenumber]);

			quadric = gluNewQuadric();
			gluSphere(quadric, 2, 30, 30);
		}
		glPopMatrix();
	}

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45, ((GLfloat)width / (GLfloat)height), 0.1, 100.0);
}

void uninitialize()
{
	//uninitialize code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE |
			SWP_NOOWNERZORDER |
			SWP_NOZORDER |
			SWP_NOSIZE
			| SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);

	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;
}