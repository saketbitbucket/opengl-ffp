#include <windows.h>
#include <stdio.h> // for FILE I/O
#include <gl\glew.h> // for GLSL extensions IMPORTANT : This Line Should Be Before #include<gl\gl.h> And #include<gl\glu.h>
#include <gl/GL.h>
#include "vmath.h"

#include "Sphere.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphereelements[2280];

GLuint gVao;
GLuint gVboposition;
GLuint gVbonormal;
GLuint gVboelement;

bool gbLight;
bool gbPerVertexPhong;
bool gbPerFragmentPhong;

float gAngle;
void updateAngle(void)
{
	gAngle += 0.0001;
	if (gAngle > 360.0)
		gAngle = 0.0;
}


GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;
GLuint V_KeyPressed_uniform;

/////////////////////////////////////////////////////
GLuint La0_uniform;
GLuint Ld0_uniform;
GLuint Ls0_uniform;
GLuint light0_position_uniform;

GLuint La1_uniform;
GLuint Ld1_uniform;
GLuint Ls1_uniform;
GLuint light1_position_uniform;

GLuint La2_uniform;
GLuint Ld2_uniform;
GLuint Ls2_uniform;
GLuint light2_position_uniform;
//////////////////////////////////////////////////////

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

////////////////////////////////////////////////////////////////////
//LIGHT0
GLfloat light0_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat light0_diffuse[] = { 1.0f,0.0f, 0.0f, 0.0f };
GLfloat light0_specular[] = { 1.0f,0.0f, 0.0f, 0.0f };
//GLfloat light0_position[] = { 2.0f, 1.0f, 1.0f };
GLfloat light0_position[] = { 0.0f, 0.0f, 2.0f };


//LIGHT1
GLfloat light1_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat light1_diffuse[] = { 0.0f,0.0f, 1.0f, 0.0f };
GLfloat light1_specular[] = { 0.0f,0.0f, 1.0f, 0.0f };
//GLfloat light1_position[] = { -2.0f, 1.0f, 1.0f };
GLfloat light1_position[] = { 0.0f, 0.0f, 2.0f };


//LIGHT2
GLfloat light2_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat light2_diffuse[] = { 0.0f,1.0f, 0.0f, 0.0f };
GLfloat light2_specular[] = { 0.0f,1.0f, 0.0f, 0.0f };
GLfloat light2_position[] = { 0.0f, 0.0f, 2.0f };


//Material
GLfloat material_ambient[] = { 0.0f,0.0f, 0.0f, 0.0f };
GLfloat material_diffuse[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat material_specular[] = { 1.0f,1.0f, 1.0f, 1.0f };
GLfloat material_shininess = 50.0f ;

/////////////////////////////////////////////////////////////////////









// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	void initialize(void);
	void uninitialize(void);
	void display(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// registering class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szClassName,
		TEXT("PP Three lights on static sphere"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize
	initialize();

	// message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			updateAngle();

			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case 0x51:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case VK_ESCAPE: // for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;

		//case 0x41: // for 'A' or 'a'
		//	if (bIsAKeyPressed == false)
		//	{
		//		gbAnimate = true;
		//		bIsAKeyPressed = true;
		//	}
		//	else
		//	{
		//		gbAnimate = false;
		//		bIsAKeyPressed = false;
		//	}
		//	break;

		case 0x4C: // for 'L' or 'l'
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;



		case 0x56: // for 'V' or 'v'
			if (gbPerVertexPhong == false)
			{
				gbPerVertexPhong = true;
				gbPerFragmentPhong = false;
			}
			break;

		case 0x46: // for 'F' or 'f'
			if (gbPerFragmentPhong == false)
			{
				gbPerVertexPhong = false;
				gbPerFragmentPhong = true;
			}
			break;


		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

struct Point
{
	double X;
	double Y;
	double Z;
};
Point findNormalFromThreePoints(Point p1, Point p2, Point p3)
{
	vmath::dvec3 vector1 = { p1.X - p2.X, p1.Y - p2.Y,  p1.Z - p2.Z };
	vmath::dvec3 vector2 = { p3.X - p2.X, p3.Y - p2.Y,  p3.Z - p2.Z };
	vmath::dvec3 normalVector = cross(vector1,vector2);
	//normalize(normalVector);
	Point normal = {normalVector[0],normalVector[1],normalVector[2] };
	return normal;
}

void initialize(void)
{
	// function declarations
	void uninitialize(void);
	void resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	// choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW Initialization Code For GLSL ( IMPORTANT : It Must Be Here. Means After Creating OpenGL Context But Before Using Any OpenGL Function )
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER ***
	// create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =


	



		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform int u_lighting_enabled;" \
		"uniform int u_perVertexPhong_enabled;" \
	
		"uniform vec3 u_La0;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec4 u_light0_position;" \
	
		"uniform vec3 u_La1;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec4 u_light1_position;" \

		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec4 u_light2_position;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"out vec3 phong_ads_color;" \

		"out vec3 transformed_normals;" \
		"out vec3 light0_direction;" \
		"out vec3 viewer_vector0;" \

		"out vec3 light1_direction;" \
		"out vec3 viewer_vector1;" \
		
		"out vec3 light2_direction;" \
		"out vec3 viewer_vector2;" \

		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \


			"if(u_perVertexPhong_enabled==1)"\
			"{" \
				"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
				"vec3 transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \

				"vec3 light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);" \
				"vec3 light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);" \
				"vec3 light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);" \

				"float tn_dot_ld0 = max(dot(transformed_normals, light0_direction),0.0);" \
				"float tn_dot_ld1 = max(dot(transformed_normals, light1_direction),0.0);" \
				"float tn_dot_ld2 = max(dot(transformed_normals, light2_direction),0.0);" \

				"vec3 ambient0 = u_La0 * u_Ka;" \
				"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
				"vec3 reflection_vector0 = reflect(-light0_direction, transformed_normals);" \
				"vec3 viewer_vector0 = normalize(-eye_coordinates.xyz);" \
				"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, viewer_vector0), 0.0), u_material_shininess);" \
		
				"vec3 ambient1 = u_La1 * u_Ka;" \
				"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
				"vec3 reflection_vector1 = reflect(-light1_direction, transformed_normals);" \
				"vec3 viewer_vector1 = normalize(-eye_coordinates.xyz);" \
				"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, viewer_vector1), 0.0), u_material_shininess);" \

				"vec3 ambient2 = u_La2 * u_Ka;" \
				"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
				"vec3 reflection_vector2 = reflect(-light2_direction, transformed_normals);" \
				"vec3 viewer_vector2 = normalize(-eye_coordinates.xyz);" \
				"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, viewer_vector2), 0.0), u_material_shininess);" \
		
				"phong_ads_color=ambient0 + diffuse0 + specular0+ambient1 + diffuse1 + specular1 + ambient2 + diffuse2 + specular2;" \
			"}" \
		
			"else" \
			"{" \

				"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
				"transformed_normals=normalize(mat3(u_view_matrix * u_model_matrix) * vNormal);" \

				"light0_direction = normalize(vec3(u_light0_position) - eye_coordinates.xyz);" \
				"light1_direction = normalize(vec3(u_light1_position) - eye_coordinates.xyz);" \
				"light2_direction = normalize(vec3(u_light2_position) - eye_coordinates.xyz);" \
					
				"viewer_vector0 = normalize(-eye_coordinates.xyz);" \
				"viewer_vector1 = normalize(-eye_coordinates.xyz);" \
				"viewer_vector2 = normalize(-eye_coordinates.xyz);" \
				"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
			"}" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		
		
		"#version 130" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light0_direction;" \
		"in vec3 viewer_vector0;" \
		"in vec3 light1_direction;" \
		"in vec3 viewer_vector1;" \
		"in vec3 light2_direction;" \
		"in vec3 viewer_vector2;" \
		"uniform int u_lighting_enabled;" \
		"uniform int u_perVertexPhong_enabled;" \
		"uniform vec3 u_La0;" \
		"uniform vec3 u_Ld0;" \
		"uniform vec3 u_Ls0;" \
		"uniform vec4 u_light0_position;" \

		"uniform vec3 u_La1;" \
		"uniform vec3 u_Ld1;" \
		"uniform vec3 u_Ls1;" \
		"uniform vec4 u_light1_position;" \

		"uniform vec3 u_La2;" \
		"uniform vec3 u_Ld2;" \
		"uniform vec3 u_Ls2;" \
		"uniform vec4 u_light2_position;" \

		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"out vec4 FragColor;" \
		"in vec3 phong_ads_color;" \
		"void main(void)" \
		"{" \

			"if(u_lighting_enabled==1)" \
			"{" \

				"if(u_perVertexPhong_enabled==1)"\
				"{" \
					"FragColor = vec4(phong_ads_color, 1.0);" \
				"}" \
				"else"\
				"{" \
					"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
					"vec3 normalized_light0_direction=normalize(light0_direction);" \
					"vec3 normalized_viewer_vector0=normalize(viewer_vector0);" \
					"vec3 normalized_light1_direction=normalize(light1_direction);" \
					"vec3 normalized_viewer_vector1=normalize(viewer_vector1);" \
					"vec3 normalized_light2_direction=normalize(light2_direction);" \
					"vec3 normalized_viewer_vector2=normalize(viewer_vector2);" \
		
					"vec3 ambient0 = u_La0 * u_Ka;" \
					"float tn_dot_ld0 = max(dot(normalized_transformed_normals, normalized_light0_direction),0.0);" \
					"vec3 diffuse0 = u_Ld0 * u_Kd * tn_dot_ld0;" \
					"vec3 reflection_vector0 = reflect(-normalized_light0_direction, normalized_transformed_normals);" \
					"vec3 specular0 = u_Ls0 * u_Ks * pow(max(dot(reflection_vector0, normalized_viewer_vector0), 0.0), u_material_shininess);" \
				
					"vec3 ambient1 = u_La1 * u_Ka;" \
					"float tn_dot_ld1 = max(dot(normalized_transformed_normals, normalized_light1_direction),0.0);" \
					"vec3 diffuse1 = u_Ld1 * u_Kd * tn_dot_ld1;" \
					"vec3 reflection_vector1 = reflect(-normalized_light1_direction, normalized_transformed_normals);" \
					"vec3 specular1 = u_Ls1 * u_Ks * pow(max(dot(reflection_vector1, normalized_viewer_vector1), 0.0), u_material_shininess);" \

					"vec3 ambient2 = u_La2 * u_Ka;" \
					"float tn_dot_ld2 = max(dot(normalized_transformed_normals, normalized_light2_direction),0.0);" \
					"vec3 diffuse2 = u_Ld2 * u_Kd * tn_dot_ld2;" \
					"vec3 reflection_vector2 = reflect(-normalized_light2_direction, normalized_transformed_normals);" \
					"vec3 specular2 = u_Ls2 * u_Ks * pow(max(dot(reflection_vector2, normalized_viewer_vector2), 0.0), u_material_shininess);" \
					"vec3 phong_ads_color1=ambient0 + diffuse0 + specular0+ambient1 + diffuse1 + specular1+ambient2 + diffuse2 + specular2;" \
					"FragColor = vec4(phong_ads_color1, 1.0);" \
				"}" \
			"}" \
			"else" \
			"{" \
				"FragColor = vec4(phong_ads_color, 1.0);" \
			"}" \
		"}";
	
	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// L/l key is pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// V/v key is pressed or not
	V_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_perVertexPhong_enabled");

	// ambient color intensity of light
	La0_uniform = glGetUniformLocation(gShaderProgramObject, "u_La0");
	// diffuse color intensity of light
	Ld0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld0");
	// specular color intensity of light
	Ls0_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls0");
	// position of light
	light0_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light0_position");;


	// ambient color intensity of light
	La1_uniform = glGetUniformLocation(gShaderProgramObject, "u_La1");
	// diffuse color intensity of light
	Ld1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld1");
	// specular color intensity of light
	Ls1_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls1");
	// position of light
	light1_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light1_position");;



	// ambient color intensity of light
	La2_uniform = glGetUniformLocation(gShaderProgramObject, "u_La2");
	// diffuse color intensity of light
	Ld2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld2");
	// specular color intensity of light
	Ls2_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls2");
	// position of light
	light2_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light2_position");;




	// ambient reflective color intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");

	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphereelements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();


	// position vbo
	glGenBuffers(1, &gVboposition);
	glBindBuffer(GL_ARRAY_BUFFER, gVboposition);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbonormal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbonormal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVboelement);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVboelement);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphereelements), sphereelements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black

	// set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;
	gbPerVertexPhong = true;

	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}


void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
	glUseProgram(gShaderProgramObject);

	if (gbLight == true)
	{
		/////////////////////////////////////
		double x = 10.0*cos(gAngle), y = 10.0*sin(gAngle);
		light0_position[0] =0 ;
		light0_position[1] = x;
		light0_position[2] = y;

		light1_position[0] = x;
		light1_position[1] = 0;
		light1_position[2] = y;

		light2_position[0] = x;
		light2_position[1] = y;
		light2_position[2] = 0;
		/////////////////////////////////////

		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 1);
		if(gbPerVertexPhong==true)
			glUniform1i(V_KeyPressed_uniform, 1);
		else
			glUniform1i(V_KeyPressed_uniform, 0);

		// setting light's properties
		glUniform3fv(La0_uniform, 1, light0_ambient);
		glUniform3fv(Ld0_uniform, 1, light0_diffuse);
		glUniform3fv(Ls0_uniform, 1, light0_specular);
		glUniform4fv(light0_position_uniform, 1, light0_position);

		glUniform3fv(La1_uniform, 1, light1_ambient);
		glUniform3fv(Ld1_uniform, 1, light1_diffuse);
		glUniform3fv(Ls1_uniform, 1, light1_specular);
		glUniform4fv(light1_position_uniform, 1, light1_position);

		glUniform3fv(La2_uniform, 1, light2_ambient);
		glUniform3fv(Ld2_uniform, 1, light2_diffuse);
		glUniform3fv(Ls2_uniform, 1, light2_specular);
		glUniform4fv(light2_position_uniform, 1, light2_position);

		// setting material's properties
		glUniform3fv(Ka_uniform, 1, material_ambient);
		glUniform3fv(Kd_uniform, 1, material_diffuse);
		glUniform3fv(Ks_uniform, 1, material_specular);
		glUniform1f(material_shininess_uniform, material_shininess);
	}
	else
	{
		// set 'u_lighting_enabled' uniform
		glUniform1i(L_KeyPressed_uniform, 0);
		glUniform1i(V_KeyPressed_uniform, 0);
	}

	// OpenGL Drawing
	// set all matrices to identity
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();

	modelMatrix = translate(0.0f, 0.0f, -2.0f);
	
	glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
	glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
	glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

	// *** bind vao ***
	glBindVertexArray(gVao);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVboelement);
	glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}
void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}

	// destroy vao
	if (gVao)
	{
		glDeleteVertexArrays(1, &gVao);
		gVao = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment  shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	//Deselect the rendering context
	wglMakeCurrent(NULL, NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
