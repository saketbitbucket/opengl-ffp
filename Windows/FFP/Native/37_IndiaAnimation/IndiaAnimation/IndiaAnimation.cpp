/****************************************************INDIA ANIMATION***************************************************************/
/***********************************This program demonstrates an animation of text "INDIA"*****************************************/

//This text is not a opengl text, It is a geometry made of GL_QUARDS.



/************************************************Logic for Drawing quard**********************************************************/
//There are three types of lines used to draw the text -
//Horizontal, Vertical and Slant (can be drawn by rotating horizontal or vertical line)
//We are drawing this horizontal line with a distance width/10 distanc apart
//After drawing horizontal line we are changing its orientation to make it Slant




/*****************************************************Logic for Animation********************************************************/
/*Draw one geometry at a time , after finishing first draw next. At the end draw all together*/
/*Move the geometries slowly so that it gives an appearance like animation*/



/***********************************************Technical/Implementation details************************************************/
//We are drawing a small size rectange at the origine (width/10)
//We are the using transfrmation to give it desiered shape and orientation
//Objective is to understand GLRotate, GLTranslate, GlScale along with GLpushMatrix and GlPopMatrix

//Good to Learn:
//Each draw function draws the geometry using MV matrix but doesn't changes it.
// Push matrix first, then do the changes and afterward pop it.

//Good to Learn:
//The order of Scaling and Translation - it matters (like the BODMAS rule - first mult then add).

//FURTHER Optimizations:
//Note: Here Descaling is perfromed as scaling object but with less value (all factor is +ve and greater than 1)
//Remove excess variables and functions
//All hard code values are in proportion with window height and width

#include <Windows.h>
#include <gl/GL.h>
#include <gl/GLU.h>

#define WIN_WIDTH  800
#define WIN_HEIGHT 800

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"Winmm.lib")
#pragma comment(lib,"glu32.lib")


//Prototype Of WndProc() declared Globally
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Global variable decleration
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullScreen = false;

//Variables 

float thickness = 0.0f;

bool AnimationOfI1Done = false;
bool AnimationOfI2Done = false;
bool AnimationOfNDone = false;
bool AnimationOfDDone = false;
bool AnimationOfADone = false;
bool AnimationOfTriColorDone = false;
bool DeScalingOfTriColorDone = false;

float horizontalSpeed = 0.2 / 10000.0f; //0.2 - horizonatl distance to travel
float verticalSpeed = 1.5 / 10000.0f; //0.8 - Vertical distance to travel
float DecompressionSpeed = 0.001;
float GlowingSpeed = 6000.0f;

float translationI1 = 0.0f;
float translationN = 0.0f;
float translationD = 0.0f;
float translationI2 = 0.0f;
float translationA = 0.0f;
float translationTriColor = 0.0f;

float deScaleTriColor = 0.0f;

//RGB of colors

float orangeR = 255.0f/255.0f; float orangeG = 153.0f/255.0f; float orangeB = 51.0f/255.0f;
float WhiteR = 1.0f, WhiteG = 1.0f, WhiteB = 1.0f;
float GreenR = (0.0f / 255.0f); float GreenG = (128.0f / 255.0f); float GreenB = (0.0f / 255.0f);

float orangeR_forD = 0.0f; float orangeG_forD = 0.0f; float orangeB_forD = 0.0f;
float GreenR_forD = 0.0f; float GreenG_forD = 0.0f; float GreenB_forD = 0.0f;


//Main()
//Functions -- drawing object on origin
void updateTriColor()
{
	translationTriColor += (0.2 / 1000.0);
}

void updateI1()
{
	translationI1 += horizontalSpeed;
}

void updateN()
{
	translationN += verticalSpeed;
}

void updateD()
{
	//Update orange
	orangeR_forD += ((245.0f / GlowingSpeed)/255);
	orangeG_forD += ((143.0f / GlowingSpeed) / 255);
	orangeB_forD += ((44.0f / GlowingSpeed) / 255);

	GreenR_forD += ((0.0f / GlowingSpeed) / 255);
	GreenG_forD += ((128.0f / GlowingSpeed) / 255);
	GreenB_forD += ((0.0f / GlowingSpeed) / 255);
}

void updateI2()
{
	translationI2 += verticalSpeed;
}

void updateA()
{
	translationA += horizontalSpeed;
}

//Loacal variable same as name of global
void DrawMulticolorRect(float orangeR, float orangeG, float orangeB, float GreenR, float GreenG, float GreenB)
{
	glBegin(GL_QUADS);
	{
		glColor3f(orangeR, orangeG, orangeB);
		glVertex3f(-0.1f, 0.1f, 0.0);
		glColor3f(GreenR, GreenG, GreenB);
		glVertex3f(-0.1f, -0.1f, 0.0);
		glColor3f(GreenR, GreenG, GreenB);
		glVertex3f(0.1f, -0.1f, 0.0);
		glColor3f(orangeR, orangeG, orangeB);
		glVertex3f(0.1f, 0.1f, 0.0);
	}glEnd();

}

void DrawSinglecolorRect()  //Color will be set outside the function
{
	glBegin(GL_QUADS);
	{
		glVertex3f(-0.1f, 0.1f, 0.0);
		glVertex3f(-0.1f, -0.1f, 0.0);
		glVertex3f(0.1f, -0.1f, 0.0);
		glVertex3f(0.1f, 0.1f, 0.0);
	}glEnd();

}

void DrawI()
{
	glPushMatrix(); //save the initial orientation of matrix
	glScalef(0.04, 8, 1);
	DrawMulticolorRect(orangeR, orangeG, orangeB, GreenR, GreenG, GreenB);
	glPopMatrix(); //Set back initial matrix
}

void DrawN()
{
	//Space of two vertical lines is 1/10 of the screen width (it is 2 i.e -1 to 1)

	glPushMatrix(); //Save the starting axis location

	glScalef(0.04, 8, 1);
	DrawMulticolorRect(orangeR, orangeG, orangeB, GreenR, GreenG, GreenB);

	glPopMatrix(); //go to starting location of axis

	glPushMatrix();//Save initial axis location

	glTranslatef(0.1f, 0, 0);
	glRotatef(7.0f, 0, 0, 1); //Angle of roatation came from trial n error can be calculated using vector angle
	glScalef(0.04, 8.1, 1);
	DrawMulticolorRect(orangeR, orangeG, orangeB, GreenR, GreenG, GreenB);

	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0, 0);
	glScalef(0.04, 8, 1);
	DrawMulticolorRect(orangeR, orangeG, orangeB, GreenR, GreenG, GreenB);

	glPopMatrix();
}

void DrawD()
{
	glPushMatrix();
	glScalef(0.04, 8, 1);
	DrawMulticolorRect(orangeR_forD, orangeG_forD, orangeB_forD, GreenR_forD, GreenG_forD, GreenB_forD); // | -Drawing vertical line
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0, 0); // move 1/10 of the screen width
	glScalef(0.04, 8, 1);
	DrawMulticolorRect(orangeR_forD, orangeG_forD, orangeB_forD, GreenR_forD, GreenG_forD, GreenB_forD);  // | | -Drawing parallel vertical line
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.082f, 0.8f, 0); // move 1/10 of the screen width
	glScalef(1.20, 0.06, 1);
	glColor3f(orangeR_forD, orangeG_forD, orangeB_forD);
	DrawSinglecolorRect(); // draw horizontal line
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.082f, -0.8f, 0); // move 1/10 of the screen width
	glScalef(1.20, 0.06, 1);
	glColor3f(GreenR_forD, GreenG_forD, GreenB_forD);
	DrawSinglecolorRect();  // draw horizontal line
	glPopMatrix();

}

void DrawA()
{
	glPushMatrix();

	glRotatef(-7.0f, 0, 0, 1);
	glScalef(0.04, 8.1, 1);
	DrawMulticolorRect(orangeR, orangeG, orangeB, GreenR, GreenG, GreenB);
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.2f, 0, 0);
	glRotatef(7.0f, 0, 0, 1);
	glScalef(0.04, 8.1, 1);
	DrawMulticolorRect(orangeR, orangeG, orangeB, GreenR, GreenG, GreenB);
	glPopMatrix();

}

void DrawTriColorLine()
{
	
	glPushMatrix();
	glTranslatef(0.0, 0.018, 0.0);
	glScalef(1.0f, 0.03, 1.0f);
	glColor3f(255.0f / 255.0f, 153.0f / 255.0f, 51.0f / 255.0f); //orange
	DrawSinglecolorRect();
	glPopMatrix();

	glPushMatrix();
	glScalef(1.0f, 0.035, 1);
	glColor3f(1.0f, 1.0f, 1.0f); //white
	DrawSinglecolorRect();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(0.0, -0.020, 0.0);
	glScalef(1.0f, 0.05, 1.0f);
	glColor3f(0.0f / 255.0f, 128.0f / 255.0f, 0.0f / 255.0f); //green
	DrawSinglecolorRect();
	glPopMatrix();
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//Function Prototype
	void initialize(void);
	void uninitialize(void);
	void display(void);

	//Variable decleration

	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;

	TCHAR szClassName[] = TEXT("RTROGL");
	bool bDone = false;

	//Code
	//Initilizing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	//Register Class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szClassName,
		TEXT("OpenGL Fixed Function Pipeline using Native Windowing "),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		600,
		0,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	//initialize
	initialize();

	ShowWindow(hwnd, SW_SHOW);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	//Message Loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true)
					bDone = true;
				display();
			}
		}
	}

	uninitialize();

	return ((int)msg.wParam);
}

//Wnd Proc()

LRESULT CALLBACK WndProc(HWND hwnd, UINT imsg, WPARAM wParam, LPARAM lparam)
{
	//Function prototype
	void display(void);
	void resize(int, int);
	void ToggleFullScreen(void);
	void uninitialize(void);

	//code 
	switch (imsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;

		/*case WM_PAINT:
		display();
		break;
		return(0);*/

		/*case WM_ERASEBKGND:
		return(0);*/

	case WM_SIZE:
		resize(LOWORD(lparam), HIWORD(lparam));
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			gbEscapeKeyIsPressed = true;
			break;

		case 0x46: // F
			if (gbFullScreen == false)
			{
				ToggleFullScreen();
				gbFullScreen = true;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = false;
			}
			break;
		default:
			break;
		}
		break;

	case WM_LBUTTONDOWN:
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		break;
	}
	return(DefWindowProc(hwnd, imsg, wParam, lparam));
}
void ToggleFullScreen(void)
{
	//Variable Declerations
	MONITORINFO mi;

	//code
	if (gbFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(false);
	}
	else
	{
		//code 
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	//function prototypes
	void resize(int, int);

	//variable decleration
	PIXELFORMATDESCRIPTOR pfd;

	int ipixelFormatIndex;

	//code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	//Initialize
	pfd.nSize = sizeof(PPIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cBlueBits = 8;
	pfd.cGreenBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32; //Changes for 3D window

	ghdc = GetDC(ghwnd);

	ipixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);

	if (ipixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (SetPixelFormat(ghdc, ipixelFormatIndex, &pfd) == FALSE)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);


	resize(WIN_WIDTH, WIN_HEIGHT);

	gbFullScreen = false;
	ToggleFullScreen();

	PlaySound(TEXT("Music.wav"), NULL, SND_ASYNC| SND_LOOP);
}

//Logic:
//Looking at the TEXT INDIA it is visible that horizontally screen is divided in 10 parts

void display(void)
{
	//code 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Rendering Command
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	if (AnimationOfI1Done == false && translationI1 < 0.2)
	{
	  glTranslatef(-1.0f, 0, 0); //go to extreme left of window
	  glTranslatef(translationI1, 0, 0);
	  updateI1();
	}
	else
	{
		//Done animating make geometry still
		glTranslatef(-0.8, 0, 0);
		AnimationOfI1Done = true;
	}
	DrawI();

	if (AnimationOfI1Done == true)
	{
		if (AnimationOfDDone == false && translationN < 2.0)
		{
			glTranslatef(0.2, 2.0f, 0);
			glTranslatef(0, -translationN, 0);
			updateN();
		}
		else
		{
			glLoadIdentity();
			glTranslatef(-0.6, 0, 0); //move to width/10 towards +x : note Draw I doesn't change orientation of axis
			AnimationOfNDone = true;
		}
		DrawN();
	}

	if (AnimationOfI1Done == true && AnimationOfNDone == true)
	{       
		if (AnimationOfDDone == false && orangeR_forD < (245.0f/255.0f))
		{
			updateD();
		}
		else
		{
			AnimationOfDDone = true;
		}
		glLoadIdentity();
		glTranslatef(-0.2, 0, 0);
		DrawD();
	}

	if (AnimationOfI1Done == true && AnimationOfNDone == true && AnimationOfDDone == true)
	{
		if(AnimationOfI2Done == false && translationI2 < 2.0)
		{
			glTranslatef(0.4, -2.0, 0);
			glTranslatef(0, translationI2, 0);
			updateI2();
		}
	    else
     	{
			glLoadIdentity();
			glTranslatef(0.2, 0, 0.0);
			AnimationOfI2Done = true;
	    }
		DrawI();
	}

	if (AnimationOfI1Done == true && 
		AnimationOfNDone == true && 
		AnimationOfDDone == true &&
		AnimationOfI2Done == true)
	{
		if (AnimationOfADone == false && translationA < 0.5)
		{
			glTranslatef(0.8, 0, 0);
			glTranslatef(-translationA, 0, 0);
			updateA();
		}
		else
		{
			glLoadIdentity();
			glTranslatef(0.5, 0, 0);
			AnimationOfADone = true;
		}
		DrawA();
	}

	if (AnimationOfI1Done == true &&
		AnimationOfNDone == true &&
		AnimationOfDDone == true &&
		AnimationOfI2Done == true &&
		AnimationOfADone == true)
	{
		//Tricolor line
		if (AnimationOfTriColorDone == false && translationTriColor < 1.70f) //move left to rigth
		{
			glLoadIdentity();
			//Now move the tri color line
			glTranslatef(translationTriColor, 0, 0);

			glTranslatef(-1.85, 0, 0.0);  //Shift the end point outside the screen, 1.85 is distance travelled by end point (-1 to 0.85)
			glScalef(8.5, 1, 1); // scale it by 8.5 because right leg of A is at 0.7 and then this vertical leg is rotated.
						
			updateTriColor();
			
		}
	    else 
		{
			AnimationOfTriColorDone = true;
			
			//Draw the tri color line at the final position of linear animation towards x -- done in above if at end.
			glLoadIdentity();
			glTranslatef(1.70, 0, 0);
			glTranslatef(-1.85f, 0, 0.0);  //Shift the end point outside the screen, 1.8 is distance travelled by end point
			glScalef(8.5, 1, 1); // scale it by 8.5 because right leg of A is at 0.7 (line legth should be 1.70)and then this vertical leg is rotated.

			//Now we are at a point where we have moved tri color line from extreme left to right(desired location)

			//perform the -- shrink right to left operation (line seems to be reducing size from right only)
            //scale down so that scale factor comes to oiginal i.e 1.
			//and translate along right, so that no movement of right end appears.

			//Note: Here Descaling is perfromed as scaling object but with less value (all factor is +ve and greater than 1)
			if(DeScalingOfTriColorDone == false && deScaleTriColor < 7.5f) //we have to scale down to original size i.e scale down by 7.
			{
				glLoadIdentity();
				//Using immediate variable for redability -- this is assignment not product
				float original_length = 0.2; //(x=-0.1 to x=0.1)
				float scaled_length = 0.2 * 8.5;
				float descaled_length = (0.2*(8.5 - deScaleTriColor));

				float translation_length = (scaled_length - descaled_length) / 2;//reduced length after decaling / 2
				glTranslatef(translation_length, 0, 0);
				glTranslatef(-0.15f, 0, 0.0); //so that it appears at end to A -- same as translate -1.8 and again +1.70.
				glScalef((8.5 - deScaleTriColor), 1, 1); // scale it by 8.5 because right leg of A is at 0.7 and then this vertical leg is rotated.

				deScaleTriColor += DecompressionSpeed;
			}
			else
			{
				glLoadIdentity();
				glTranslatef(0.6, 0, 0.0);
				PlaySound(NULL, NULL, SND_PURGE);
			}		
		}
		DrawTriColorLine();
	}
				
	//glFlush(); -- Commented single buffer api
	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(0, ((GLfloat)width / (GLfloat)height), 0.1, 100.0);
}

void uninitialize()
{
	//uninitialize code

	if (gbFullScreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0,
			SWP_NOMOVE |
			SWP_NOOWNERZORDER |
			SWP_NOZORDER |
			SWP_NOSIZE
			| SWP_FRAMECHANGED);

		ShowCursor(true);
	}

	wglMakeCurrent(NULL, NULL);

	wglDeleteContext(ghrc);

	ghrc = NULL;

	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	//PlaySound(NULL , NULL, SND_PURGE);
}