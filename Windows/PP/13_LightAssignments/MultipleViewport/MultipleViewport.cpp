#include <windows.h>
#include <stdio.h> // for FILE I/O

#include <gl\glew.h> // for GLSL extensions IMPORTANT : This Line Should Be Before #include<gl\gl.h> And #include<gl\glu.h>

#include <gl/GL.h>

#include "vmath.h"

#include "Sphere.h"

#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#pragma comment(lib,"Sphere.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

using namespace vmath;

enum
{
	VDG_ATTRIBUTE_VERTEX = 0,
	VDG_ATTRIBUTE_COLOR,
	VDG_ATTRIBUTE_NORMAL,
	VDG_ATTRIBUTE_TEXTURE0,
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
FILE *gpFile = NULL;

HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;

DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };

bool gbActiveWindow = false;
bool gbEscapeKeyIsPressed = false;
bool gbFullscreen = false;

GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint gNumElements;
GLuint gNumVertices;
float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];

int dx = 2, dy = 2;
double angle1 = 0.0;
bool gbX = true;
bool gbY = true;
bool gbZ = true;

GLuint gVao_sphere;
GLuint gVbo_sphere_position;
GLuint gVbo_sphere_normal;
GLuint gVbo_sphere_element;

GLuint model_matrix_uniform, view_matrix_uniform, projection_matrix_uniform;

GLuint L_KeyPressed_uniform;

GLuint La_uniform;
GLuint Ld_uniform;
GLuint Ls_uniform;
GLuint light_position_uniform;

GLuint Ka_uniform;
GLuint Kd_uniform;
GLuint Ks_uniform;
GLuint material_shininess_uniform;

mat4 gPerspectiveProjectionMatrix;

bool gbLight;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat lightPosition[] = { 100.0f,100.0f,100.0f,1.0f };

////////added by abhi
GLfloat data[312] = {

	// ***** 1st sphere on 1st column, emerald *****
	// ambient material
	0.0215, // r
	0.1745, // g
	0.0215, // b
	1.0f,   // a

	// diffuse material
	0.07568, // r
	0.61424, // g
	0.07568, // b
	1.0f,    // a

	// specular material
	0.633,    // r
	0.727811, // g
	0.633,    // b
	1.0f,     // a

	// shininess
	0.6 * 128,

	// ***** 2nd sphere on 1st column, jade *****
	// ambient material
	0.135,  // r
	0.2225, // g
	0.1575, // b
	1.0f,   // a

	// diffuse material
	0.54, // r
	0.89, // g
	0.63, // b
	1.0f, // a

	// specular material
	0.316228, // r
	0.316228, // g
	0.316228, // b
	1.0f,     // a

	// shininess
	0.1 * 128,

	// ***** 3rd sphere on 1st column, obsidian *****
	// ambient material
	0.05375, // r
	0.05,    // g
	0.06625, // b
	1.0f,    // a

	// diffuse material
	0.18275, // r
	0.17,    // g
	0.22525, // b
	1.0f,    // a

	// specular material
	0.332741, // r
	0.328634, // g
	0.346435, // b
	1.0f,     // a

			// shininess
			0.3 * 128,

			// ***** 4th sphere on 1st column, pearl *****
			// ambient material
			0.25,    // r
			0.20725, // g
			0.20725, // b
			1.0f,    // a

		// diffuse material
		1.0,   // r
		0.829, // g
		0.829, // b
		1.0f,  // a

		// specular material
		0.296648, // r
		0.296648, // g
		0.296648, // b
		1.0f,     // a

		// shininess
		0.088 * 128,

		// ***** 5th sphere on 1st column, ruby *****
		// ambient material
		0.1745,  // r
		0.01175, // g
		0.01175, // b
		1.0f,    // a


			// diffuse material
			0.61424, // r
			0.04136, // g
			0.04136, // b
			1.0f,    // a


			// specular material
			0.727811, // r
			0.626959, // g
			0.626959, // b
			1.0f,     // a

						// shininess
			0.6 * 128,

			// ***** 6th sphere on 1st column, turquoise *****
			// ambient material
			0.1,     // r
			0.18725, // g
			0.1745,  // b
			1.0f,    // a


			// diffuse material
			0.396,   // r
			0.74151, // g
			0.69102, // b
			1.0f,    // a

		    
			// specular material
			0.297254, // r
			0.30829,  // g
			0.306678, // b
			1.0f,     // a

			// shininess
			0.1 * 128,

			// ***** 1st sphere on 2nd column, brass *****
			// ambient material
			0.329412, // r
			0.223529, // g
			0.027451, // b
			1.0f,     // a


			// diffuse material
			0.780392, // r
			0.568627, // g
			0.113725, // b
			1.0f,     // a

		    
			// specular material
			0.992157, // r
			0.941176, // g
			0.807843, // b
			1.0f,     // a

			// shininess
			0.21794872 * 128,

			// ***** 2nd sphere on 2nd column, bronze *****
			// ambient material
			0.2125, // r
			0.1275, // g
			0.054,  // b
			1.0f,   // a


		// diffuse material
		0.714,   // r
		0.4284,  // g
		0.18144, // b
		1.0f,    // a


		// specular material
		0.393548, // r
		0.271906, // g
		0.166721, // b
		1.0f,     // a

		// shininess
		0.2 * 128,

		// ***** 3rd sphere on 2nd column, chrome *****
		// ambient material
		0.25, // r
		0.25, // g
		0.25, // b
		1.0f, // a


		// diffuse material
		0.4,  // r
		0.4,  // g
		0.4,  // b
		1.0f, // a


		// specular material
		0.774597, // r
		0.774597, // g
		0.774597, // b
		1.0f,     // a

		// shininess
		0.6 * 128,

		// ***** 4th sphere on 2nd column, copper *****
		// ambient material
		0.19125, // r
		0.0735,  // g
		0.0225,  // b
		1.0f,    // a


					// diffuse material
																														0.7038,  // r
																														0.27048, // g
																														0.0828,  // b
																														1.0f,    // a


																																// specular material
																																0.256777, // r
																																0.137622, // g
																																0.086014, // b
																																1.0f,     // a

																																		// shininess
																																		0.1 * 128,

																																		// ***** 5th sphere on 2nd column, gold *****
																																		// ambient material
																																		0.24725, // r
																																		0.1995,  // g
																																		0.0745,  // b
																																		1.0f,    // a


																																					// diffuse material
																																					0.75164, // r
																																					0.60648, // g
																																					0.22648, // b
																																					1.0f,    // a


																																							// specular material
																																							0.628281, // r
																																							0.555802, // g
																																							0.366065, // b
																																							1.0f,     // a

																																									// shininess
																																									0.4 * 128,


																																									// ***** 6th sphere on 2nd column, silver *****
																																									// ambient material
																																									0.19225, // r
																																									0.19225, // g
																																									0.19225, // b
																																									1.0f,    // a


																																												// diffuse material
																																												0.50754, // r
																																												0.50754, // g
																																												0.50754, // b
																																												1.0f,    // a


																																														// specular material
																																														0.508273, // r
																																														0.508273, // g
																																														0.508273, // b
																																														1.0f,     // a

																																																// shininess
																																																0.4 * 128,


																																																// ***** 1st sphere on 3rd column, black *****
																																																// ambient material
																																																0.0,  // r
																																																0.0,  // g
																																																0.0,  // b
																																																1.0f, // a


																																																		// diffuse material
																																																		0.01, // r
																																																		0.01, // g
																																																		0.01, // b
																																																		1.0f, // a


																																																			// specular material
																																																			0.50, // r
																																																			0.50, // g
																																																			0.50, // b
																																																			1.0f, // a

																																																					// shininess
																																																					0.25 * 128,


																																																					// ***** 2nd sphere on 3rd column, cyan *****
																																																					// ambient material
																																																					0.0,  // r
																																																					0.1,  // g
																																																					0.06, // b
																																																					1.0f, // a


																																																						// diffuse material
																																																						0.0,        // r
																																																						0.50980392, // g
																																																						0.50980392, // b
																																																						1.0f,       // a


																																																									// specular material
																																																									0.50196078, // r
																																																									0.50196078, // g
																																																													0.50196078, // b
																																																													1.0f,       // a

																																																																// shininess
																																																																0.25 * 128,

																																																																// ***** 3rd sphere on 2nd column, green *****
																																																																// ambient material
																																																																0.0,  // r
																																																																0.0,  // g
																																																																0.0,  // b
																																																																1.0f, // a


																																																																		// diffuse material
																																																																		0.1,  // r
																																																																		0.35, // g
																																																																		0.1,  // b
																																																																		1.0f, // a


																																																																			// specular material
																																																																			0.45, // r
																																																																			0.55, // g
																																																																			0.45, // b
																																																																			1.0f, // a

																																																																					// shininess
																																																																					0.25 * 128,

																																																																					// ***** 4th sphere on 3rd column, red *****
																																																																					// ambient material
																																																																					0.0,  // r
																																																																					0.0,  // g
																																																																					0.0,  // b
																																																																					1.0f, // a


																																																																						// diffuse material
																																																																						0.5,  // r
																																																																						0.0,  // g
																																																																						0.0,  // b
																																																																						1.0f, // a


																																																																								// specular material
																																																																								0.7,  // r
																																																																								0.6,  // g
																																																																								0.6,  // b
																																																																								1.0f, // a

																																																																									// shininess
																																																																									0.25 * 128,

																																																																									// ***** 5th sphere on 3rd column, white *****
																																																																									// ambient material
																																																																									0.0,  // r
																																																																									0.0,  // g
																																																																									0.0,  // b
																																																																									1.0f, // a


																																																																											// diffuse material
																																																																											0.55, // r
																																																																											0.55, // g
																																																																											0.55, // b
																																																																											1.0f, // a


																																																																												// specular material
																																																																												0.70, // r
																																																																												0.70, // g
																																																																												0.70, // b
																																																																												1.0f, // a

																																																																														// shininess
																																																																														0.25 * 128,

																																																																														// ***** 6th sphere on 3rd column, yellow plastic *****
																																																																														// ambient material
																																																																														0.0,  // r
																																																																														0.0,  // g
																																																																														0.0,  // b
																																																																														1.0f, // a


																																																																															// diffuse material
																																																																															0.5,  // r
																																																																															0.5,  // g
																																																																															0.0,  // b
																																																																															1.0f, // a


																																																																																	// specular material
																																																																																	0.60, // r
																																																																																	0.60, // g
																																																																																	0.50, // b
																																																																																	1.0f, // a

																																																																																		// shininess
																																																																																		0.25 * 128,

																																																																																		// ***** 1st sphere on 4th column, black *****
																																																																																		// ambient material
																																																																																		0.02, // r
																																																																																		0.02, // g
																																																																																		0.02, // b
																																																																																		1.0f, // a


																																																																																				// diffuse material
																																																																																				0.01, // r
																																																																																				0.01, // g
																																																																																				0.01, // b
																																																																																				1.0f, // a


																																																																																					// specular material
																																																																																					0.4,  // r
																																																																																					0.4,  // g
																																																																																					0.4,  // b
																																																																																					1.0f, // a

																																																																																							// shininess
																																																																																							0.078125 * 128,

																																																																																							// ***** 2nd sphere on 4th column, cyan *****
																																																																																							// ambient material
																																																																																							0.0,  // r
																																																																																							0.05, // g
																																																																																							0.05, // b
																																																																																							1.0f, // a


																																																																																								// diffuse material
																																																																																								0.4,  // r
																																																																																								0.5,  // g
																																																																																								0.5,  // b
																																																																																								1.0f, // a


																																																																																										// specular material
																																																																																										0.04, // r
																																																																																										0.7,  // g
																																																																																										0.7,  // b
																																																																																										1.0f, // a

																																																																																											// shininess
																																																																																											0.078125 * 128,


																																																																																											// ***** 3rd sphere on 4th column, green *****
																																																																																											// ambient material
																																																																																											0.0,  // r
																																																																																											0.05, // g
																																																																																											0.0,  // b
																																																																																											1.0f, // a


																																																																																													// diffuse material
																																																																																													0.4,  // r
																																																																																													0.5,  // g
																																																																																													0.4,  // b
																																																																																													1.0f, // a


																																																																																														// specular material
																																																																																														0.04, // r
																																																																																														0.7,  // g
																																																																																														0.04, // b
																																																																																														1.0f, // a

																																																																																																// shininess
																																																																																																0.078125 * 128,

																																																																																																// ***** 4th sphere on 4th column, red *****
																																																																																																// ambient material
																																																																																																0.05, // r
																																																																																																0.0,  // g
																																																																																																0.0,  // b
																																																																																																1.0f, // a


																																																																																																	// diffuse material
																																																																																																	0.5,  // r
																																																																																																	0.4,  // g
																																																																																																	0.4,  // b
																																																																																																	1.0f, // a


																																																																																																			// specular material
																																																																																																			0.7,  // r
																																																																																																			0.04, // g
																																																																																																			0.04, // b
																																																																																																			1.0f, // a

																																																																																																				// shininess
																																																																																																				0.078125 * 128,


																																																																																																				// ***** 5th sphere on 4th column, white *****
																																																																																																				// ambient material
																																																																																																				0.05, // r
																																																																																																				0.05, // g
																																																																																																				0.05, // b
																																																																																																				1.0f, // a


																																																																																																						// diffuse material
																																																																																																						0.5,  // r
																																																																																																						0.5,  // g
																																																																																																						0.5,  // b
																																																																																																						1.0f, // a


																																																																																																							// specular material
																																																																																																							0.7,  // r
																																																																																																							0.7,  // g
																																																																																																							0.7,  // b
																																																																																																							1.0f, // a

																																																																																																									// shininess
																																																																																																									0.078125 * 128,

																																																																																																									// ***** 6th sphere on 4th column, yellow rubber *****
																																																																																																									// ambient material
																																																																																																									0.05, // r
																																																																																																									0.05, // g
																																																																																																									0.0,  // b
																																																																																																									1.0f, // a


																																																																																																										// diffuse material
																																																																																																										0.5,  // r
																																																																																																										0.5,  // g
																																																																																																										0.4,  // b
																																																																																																										1.0f, // a


																																																																																																												// specular material
																																																																																																												0.7,  // r
																																																																																																												0.7,  // g
																																																																																																												0.04, // b
																																																																																																												1.0f, // a

																																																																																																													// shininess
																																																																																																																																								0.078125 * 128,
};


struct MaterialData
{
	GLfloat material_ambient[4] = { 0.0f };
	GLfloat material_diffuse[4] = { 0.0f };
	GLfloat material_specular[4] = { 0.0f };
	GLfloat material_shininess[1] = { 0.0f };
}material[24];

void initMaterial(void)
{
	int i, j = 0, ind = 0;
	for (i = 0; i < 24; i++)
	{
		for (j = 0; j < 4; j++) //Ambient
			material[i].material_ambient[j] = data[ind++];

		for (j = 0; j < 4; j++) //Diffuse
			material[i].material_diffuse[j] = data[ind++];

		for (j = 0; j < 4; j++) //Specular
			material[i].material_specular[j] = data[ind++];

		material[i].material_shininess[0] = data[ind++]; //Shininess
	}
}
/////////////////////

void UpdateAngle()
{
	angle1 += 0.001;
	if (angle1 > 360.0)
		angle1 = 0.0;
}

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	void initialize(void);
	void uninitialize(void);
	void display(void);

	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("OpenGLPP");
	bool bDone = false;

	// code
	// create log file
	if (fopen_s(&gpFile, "Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Can Not Be Created\nExitting ..."), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf(gpFile, "Log File Is Successfully Opened.\n");
	}

	// initializing members of struct WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.lpfnWndProc = WndProc;
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;

	// registering class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szClassName,
		TEXT("PP 24 Spheres"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// initialize
	initialize();

	// message loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			// rendring function
			display();
			UpdateAngle();
			if (gbActiveWindow == true)
			{
				if (gbEscapeKeyIsPressed == true) //Continuation to glutLeaveMainLoop();
					bDone = true;
			}
		}
	}

	uninitialize();

	return((int)msg.wParam);
}

// WndProc()
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void resize(int, int);
	void ToggleFullscreen(void);
	void uninitialize(void);

	// variable declarations
	static bool bIsAKeyPressed = false;
	static bool bIsLKeyPressed = false;

	// code
	switch (iMsg)
	{
	case WM_ACTIVATE:
		if (HIWORD(wParam) == 0)
			gbActiveWindow = true;
		else
			gbActiveWindow = false;
		break;
	case WM_ERASEBKGND:
		return(0);
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			if (gbEscapeKeyIsPressed == false)
				gbEscapeKeyIsPressed = true;
			break;
		case 0x46: // for 'f' or 'F'
			if (gbFullscreen == false)
			{
				ToggleFullscreen();
				gbFullscreen = true;
			}
			else
			{
				ToggleFullscreen();
				gbFullscreen = false;
			}
			break;
		case 0x4C: // for 'L' or 'l'
			if (bIsLKeyPressed == false)
			{
				gbLight = true;
				bIsLKeyPressed = true;
			}
			else
			{
				gbLight = false;
				bIsLKeyPressed = false;
			}
			break;
		case 88://X
			if (gbX == false)
				angle1 = 0;
			gbX = true; gbY = false; gbZ = false;

			break;
		case 89://Y
			if (gbY == false)
				angle1 = 0;
			gbX = false; gbY = true; gbZ = false;
			break;
		case 90://Z
			if (gbZ == false)
				angle1 = 0;
			gbX = false; gbY = false; gbZ = true;
			break;
		default:
			break;
		}
		break;
	case WM_LBUTTONDOWN:
		break;
	case WM_CLOSE:
		uninitialize();
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullscreen(void)
{
	// variable declarations
	MONITORINFO mi;

	// code
	if (gbFullscreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);
	}

	else
	{
		//code
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);
	}
}

void initialize(void)
{
	// function declarations
	void uninitialize(void);
	void resize(int, int);

	initMaterial();

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialization of structure 'PIXELFORMATDESCRIPTOR'
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	// choose a pixel format which best matches with that of 'pfd'
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// set the pixel format chosen above
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == false)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// create OpenGL rendering context
	ghrc = wglCreateContext(ghdc);
	if (ghrc == NULL)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// make the rendering context created above as current n the current hdc
	if (wglMakeCurrent(ghdc, ghrc) == false)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// GLEW Initialization Code For GLSL ( IMPORTANT : It Must Be Here. Means After Creating OpenGL Context But Before Using Any OpenGL Function )
	GLenum glew_error = glewInit();
	if (glew_error != GLEW_OK)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// *** VERTEX SHADER ***
	// create shader
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);

	// provide source code to shader
	const GLchar *vertexShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_model_matrix;" \
		"uniform mat4 u_view_matrix;" \
		"uniform mat4 u_projection_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_lighting_enabled;" \
		"out vec3 transformed_normals;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"void main(void)" \
		"{" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec4 eye_coordinates=u_view_matrix * u_model_matrix * vPosition;" \
		"transformed_normals=mat3(u_view_matrix * u_model_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position) - eye_coordinates.xyz;" \
		"viewer_vector = -eye_coordinates.xyz;" \
		"}" \
		"gl_Position=u_projection_matrix * u_view_matrix * u_model_matrix * vPosition;" \
		"}";

	glShaderSource(gVertexShaderObject, 1, (const GLchar **)&vertexShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gVertexShaderObject);
	GLint iInfoLogLength = 0;
	GLint iShaderCompiledStatus = 0;
	char *szInfoLog = NULL;
	glGetShaderiv(gVertexShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gVertexShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gVertexShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Vertex Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** FRAGMENT SHADER ***
	// create shader
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);

	// provide source code to shader
	const GLchar *fragmentShaderSourceCode =
		"#version 130" \
		"\n" \
		"in vec3 transformed_normals;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 FragColor;" \
		"uniform vec3 u_La;" \
		"uniform vec3 u_Ld;" \
		"uniform vec3 u_Ls;" \
		"uniform vec3 u_Ka;" \
		"uniform vec3 u_Kd;" \
		"uniform vec3 u_Ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_lighting_enabled;" \
		"void main(void)" \
		"{" \
		"vec3 phong_ads_color;" \
		"if(u_lighting_enabled==1)" \
		"{" \
		"vec3 normalized_transformed_normals=normalize(transformed_normals);" \
		"vec3 normalized_light_direction=normalize(light_direction);" \
		"vec3 normalized_viewer_vector=normalize(viewer_vector);" \
		"vec3 ambient = u_La * u_Ka;" \
		"float tn_dot_ld = max(dot(normalized_transformed_normals, normalized_light_direction),0.0);" \
		"vec3 diffuse = u_Ld * u_Kd * tn_dot_ld;" \
		"vec3 reflection_vector = reflect(-normalized_light_direction, normalized_transformed_normals);" \
		"vec3 specular = u_Ls * u_Ks * pow(max(dot(reflection_vector, normalized_viewer_vector), 0.0), u_material_shininess);" \
		"phong_ads_color=ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_color = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"FragColor = vec4(phong_ads_color, 1.0);" \
		"}";

	glShaderSource(gFragmentShaderObject, 1, (const GLchar **)&fragmentShaderSourceCode, NULL);

	// compile shader
	glCompileShader(gFragmentShaderObject);
	glGetShaderiv(gFragmentShaderObject, GL_COMPILE_STATUS, &iShaderCompiledStatus);
	if (iShaderCompiledStatus == GL_FALSE)
	{
		glGetShaderiv(gFragmentShaderObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength > 0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetShaderInfoLog(gFragmentShaderObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Fragment Shader Compilation Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// *** SHADER PROGRAM ***
	// create
	gShaderProgramObject = glCreateProgram();

	// attach vertex shader to shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);

	// attach fragment shader to shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject);

	// pre-link binding of shader program object with vertex shader position attribute
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_VERTEX, "vPosition");
	glBindAttribLocation(gShaderProgramObject, VDG_ATTRIBUTE_NORMAL, "vNormal");

	// link shader
	glLinkProgram(gShaderProgramObject);
	GLint iShaderProgramLinkStatus = 0;
	glGetProgramiv(gShaderProgramObject, GL_LINK_STATUS, &iShaderProgramLinkStatus);
	if (iShaderProgramLinkStatus == GL_FALSE)
	{
		glGetProgramiv(gShaderProgramObject, GL_INFO_LOG_LENGTH, &iInfoLogLength);
		if (iInfoLogLength>0)
		{
			szInfoLog = (char *)malloc(iInfoLogLength);
			if (szInfoLog != NULL)
			{
				GLsizei written;
				glGetProgramInfoLog(gShaderProgramObject, iInfoLogLength, &written, szInfoLog);
				fprintf(gpFile, "Shader Program Link Log : %s\n", szInfoLog);
				free(szInfoLog);
				uninitialize();
				exit(0);
			}
		}
	}

	// get uniform locations
	model_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_model_matrix");
	view_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_view_matrix");
	projection_matrix_uniform = glGetUniformLocation(gShaderProgramObject, "u_projection_matrix");

	// L/l key is pressed or not
	L_KeyPressed_uniform = glGetUniformLocation(gShaderProgramObject, "u_lighting_enabled");

	// ambient color intensity of light
	La_uniform = glGetUniformLocation(gShaderProgramObject, "u_La");
	// diffuse color intensity of light
	Ld_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ld");
	// specular color intensity of light
	Ls_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ls");
	// position of light
	light_position_uniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");;

	// ambient reflective color intensity of material
	Ka_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ka");
	// diffuse reflective color intensity of material
	Kd_uniform = glGetUniformLocation(gShaderProgramObject, "u_Kd");
	// specular reflective color intensity of material
	Ks_uniform = glGetUniformLocation(gShaderProgramObject, "u_Ks");
	// shininess of material ( value is conventionally between 1 to 200 )
	material_shininess_uniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");;

	// *** vertices, colors, shader attribs, vbo, vao initializations ***
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);

	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(VDG_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(VDG_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	glShadeModel(GL_SMOOTH);
	// set-up depth buffer
	glClearDepth(1.0f);
	// enable depth testing
	glEnable(GL_DEPTH_TEST);
	// depth test to do
	glDepthFunc(GL_LEQUAL);
	// set really nice percpective calculations ?
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	// We will always cull back faces for better performance
	glEnable(GL_CULL_FACE);

	// set background color
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f); // black

										  // set perspective matrix to identitu matrix
	gPerspectiveProjectionMatrix = mat4::identity();

	gbLight = false;

	// resize
	resize(WIN_WIDTH, WIN_HEIGHT);
}


void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// start using OpenGL program object
	glUseProgram(gShaderProgramObject);

	//////////////////////added by abhi ... to be deleted

	double xComp = 400.0*cos(angle1), yComp = 400 * sin(angle1);
	if (gbX == true)
	{
		//glRotatef(angle, 1.0f, 0.0f, 0.0f);
		//light_position[0] = angle;
		lightPosition[0] = 0;
		lightPosition[1] = xComp;
		lightPosition[2] = yComp;
	}
	else if (gbY == true)
	{
		//glRotatef(angle, 0.0f, 1.0f, 0.0f);
		//light_position[1] = angle;
		lightPosition[0] = yComp;
		lightPosition[1] = 30;
		lightPosition[2] = xComp;
	}
	else if (gbZ == true)
	{
		//glRotatef(angle, 0.0f, 0.0f, 1.0f);
		//light_position[2] = angle;
		lightPosition[0] = xComp;
		lightPosition[1] = yComp;
		lightPosition[2] = 0;
	}
	int x = 0, y = 0;
	mat4 modelMatrix = mat4::identity();
	mat4 viewMatrix = mat4::identity();
	for (int i = 0; i < 24; i++)
	{

		if (gbLight == true)
		{
			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform, 1);

			// setting light's properties
			glUniform3fv(La_uniform, 1, lightAmbient);
			glUniform3fv(Ld_uniform, 1, lightDiffuse);
			glUniform3fv(Ls_uniform, 1, lightSpecular);
			glUniform4fv(light_position_uniform, 1, lightPosition);

			glUniform3fv(Ka_uniform, 1, material[i].material_ambient);
			glUniform3fv(Kd_uniform, 1, material[i].material_diffuse);
			glUniform3fv(Ks_uniform, 1, material[i].material_specular);
			glUniform1f(material_shininess_uniform, *(material[i].material_shininess));
		}
		else
		{
			// set 'u_lighting_enabled' uniform
			glUniform1i(L_KeyPressed_uniform, 0);
		}

		// OpenGL Drawing
		// set all matrices to identity
		modelMatrix = mat4::identity();
		viewMatrix = mat4::identity();

		modelMatrix = translate((GLfloat)x - 1.5f * dx,2.5f * dy - (GLfloat)y, (GLfloat)-dx*50);

		glUniformMatrix4fv(model_matrix_uniform, 1, GL_FALSE, modelMatrix);
		glUniformMatrix4fv(view_matrix_uniform, 1, GL_FALSE, viewMatrix);
		glUniformMatrix4fv(projection_matrix_uniform, 1, GL_FALSE, gPerspectiveProjectionMatrix);

		// *** bind vao ***
		glBindVertexArray(gVao_sphere);

		// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
		glDrawElements(GL_TRIANGLES, gNumElements, GL_UNSIGNED_SHORT, 0);

		// *** unbind vao ***
		glBindVertexArray(0);

		if ((i + 1) % 6 == 0)
			x = (x + dx) % (4 * dx);
		y = (y + dy) % (6 * dy);
	}
	/////////////////////////////////

	// stop using OpenGL program object
	glUseProgram(0);

	SwapBuffers(ghdc);
}

void resize(int width, int height)
{
	//code
	if (height == 0)
		height = 1;
	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	//glViewport(0, 0, (GLsizei)width / 2, (GLsizei)height / 2); 
	//glViewport((GLsizei)width / 2, (GLsizei)height / 2, (GLsizei)width, (GLsizei)height);

	gPerspectiveProjectionMatrix = perspective(8.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void uninitialize(void)
{
	//UNINITIALIZATION CODE
	if (gbFullscreen == true)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);

		ShowCursor(TRUE);

	}

	// destroy vao
	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);
		gVao_sphere = 0;
	}

	// destroy position vbo
	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);
		gVbo_sphere_position = 0;
	}

	// destroy normal vbo
	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);
		gVbo_sphere_normal = 0;
	}

	// destroy element vbo
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);
		gVbo_sphere_element = 0;
	}

	// detach vertex shader from shader program object
	glDetachShader(gShaderProgramObject, gVertexShaderObject);
	// detach fragment  shader from shader program object
	glDetachShader(gShaderProgramObject, gFragmentShaderObject);

	// delete vertex shader object
	glDeleteShader(gVertexShaderObject);
	gVertexShaderObject = 0;
	// delete fragment shader object
	glDeleteShader(gFragmentShaderObject);
	gFragmentShaderObject = 0;

	// delete shader program object
	glDeleteProgram(gShaderProgramObject);
	gShaderProgramObject = 0;

	//Deselect the rendering context
	wglMakeCurrent(NULL, NULL);

	//Delete the rendering context
	wglDeleteContext(ghrc);
	ghrc = NULL;

	//Delete the device context
	ReleaseDC(ghwnd, ghdc);
	ghdc = NULL;

	if (gpFile)
	{
		fprintf(gpFile, "Log File Is Successfully Closed.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}
