#include <stdio.h>

union Student
{
   char name[50];
   float marks;
   int rollno;
} s1;

int main()
{
   s1.rollno = 1;

   printf("Enter name:\n");
   scanf("%s", s1.name);

   printf("Enter marks: \n");
   scanf("%f", &s1.marks);

   printf("Displaying\n Rollno :%d \nName :%s\n", s1.rollno,s1.name);
   printf("Salary: %.1f", s1.marks);

   return 0;
}

