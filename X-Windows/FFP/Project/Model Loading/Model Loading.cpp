#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <memory.h> 

#include <X11/Xlib.h>
#include <X11/Xutil.h> 
#include <X11/XKBlib.h>
#include <X11/keysym.h> 

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//For Reading OBJ file
#include<stdlib.h>
#include<vector>
#include <algorithm>
#include <iterator>
#include <cmath>


#define BUFFER_SIZE 256
#define S_EQUAL 0
#define NR_POINT_COORDS 3
#define NR_TEXTURE_COORDS 2
#define NR_NORMAL_COORDS 3
#define NR_FACE_TOKENS 3


#define FOY_ANGLE 45
#define ZNEAR 0.1
#define ZFAR 200.0

#define VIEWPORT_BOTTOMLEFT_X 0
#define VIEWPORT_BOTTOMLEFT_Y 0

#define MONKEYHEAD_X_SCALE_FACTOR 1.5f
#define MONKEYHEAD_Y_SCALE_FACTOR 1.5f
#define MONKEYHEAD_Z_SCALE_FACTOR 1.5f

#define START_ANGLE_POS 0.0f
#define END_ANGLE_POS 360.0f
#define MONKEYHEAD_ANGLE_INCREMENT 1.0f


//namespaces
using namespace std;

//global variable declarations
bool bFullscreen=false;
Display *gpDisplay=NULL;
XVisualInfo *gpXVisualInfo=NULL;
Colormap gColormap;
Window gWindow;
int giWindowWidth=800;
int giWindowHeight=600;

GLXContext gGLXContext;


//For Reading OBJ file

FILE *g_fp_logfile = NULL;
FILE *g_fp_meshfile = NULL;
char line[BUFFER_SIZE];

GLfloat g_rotate;

std::vector< std::vector<float> > g_vertices;
std::vector< std::vector<float> > g_texture;
std::vector< std::vector<float> > g_normals;
std::vector< std::vector<int> > g_face_tri, g_face_texture, g_face_normals;

float translate_x =0.0f;
float translate_y =0.0f;
float translate_z =0.0f;
float translateback = 0.0f;

//Function for reading data
void LoadMeshData(void);
void update(void);
void calculateModelPosition();

//entry-point function
int main(void)
{
	//function prototypes
	void CreateWindow(void);
	void ToggleFullscreen(void);
	void initialize(void);
	void display(void);
	void resize(int,int);
	void uninitialize();
	
	//variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	
	bool bDone=false;
	
	//code
	CreateWindow();

	//initialize
	initialize();
	
	//Message Loop
	XEvent event;
	KeySym keysym;
	
	while(bDone==false)
	{
		while(XPending(gpDisplay))
		{
			XNextEvent(gpDisplay,&event);
			switch(event.type)
			{
				case MapNotify:
					break;
				case KeyPress:
					keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);
				switch(keysym)
					{
						case XK_Escape:
							uninitialize();
							exit(0);
						case XK_F:
						case XK_f:
							if(bFullscreen==false)
							{
								ToggleFullscreen();
								bFullscreen=true;
							}
							else
							{
								ToggleFullscreen();
								bFullscreen=false;
							}
							break;
						default:
							break;
					}
					break;
				case ButtonPress: 
					switch(event.xbutton.button)
					{
						case 1: 
						    break;
						case 2: 
						    break;
						case 3: 
						    break;
						default:
						    break;
					}
					break;
				case MotionNotify: 
					break;
				case ConfigureNotify: 
					winWidth=event.xconfigure.width;
					winHeight=event.xconfigure.height;
					resize(winWidth,winHeight);
					break;
				case Expose: 
					break;
				case DestroyNotify:
					break;
				case 33:
					bDone=true;
					break;
				default:
					break;
			}
		
		}
		update();
                display();
	}
	return(0);
}

void CreateWindow(void)
{
	//function prorttypes
	void uninitialize(void);

	//variable declarations
	XSetWindowAttributes winAttribs;
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	static int frameBufferAttributes[]=
	{
		GLX_RGBA,
		GLX_RED_SIZE, 8,
		GLX_GREEN_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_ALPHA_SIZE, 8,
                GLX_DOUBLEBUFFER, True,
                GLX_DEPTH_SIZE, 24,
		None 
	}; 

	
	//code
	gpDisplay=XOpenDisplay(NULL);
	if(gpDisplay==NULL)
	{
		printf("ERROR : Unable To Open X Display.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	defaultScreen=XDefaultScreen(gpDisplay);
	
	gpXVisualInfo=glXChooseVisual(gpDisplay,defaultScreen,frameBufferAttributes);
		
	winAttribs.border_pixel=0;
	winAttribs.background_pixmap=0;
	winAttribs.colormap=XCreateColormap(gpDisplay, 
					    RootWindow(gpDisplay, gpXVisualInfo->screen), 
					    gpXVisualInfo->visual,
 					    AllocNone);
	gColormap=winAttribs.colormap;

	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);

	winAttribs.event_mask= ExposureMask | VisibilityChangeMask | ButtonPressMask | KeyPressMask | PointerMotionMask |
			       StructureNotifyMask;
	
	styleMask=CWBorderPixel | CWBackPixel | CWEventMask | CWColormap;
	
	gWindow=XCreateWindow(gpDisplay,
			      RootWindow(gpDisplay,gpXVisualInfo->screen),
			      0,
			      0,
			      giWindowWidth,
			      giWindowHeight,
			      0,
			      gpXVisualInfo->depth,
			      InputOutput,
			      gpXVisualInfo->visual,
			      styleMask,
			      &winAttribs);
	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window.\nExitting Now...\n");
		uninitialize();
		exit(1);
	}
	
	XStoreName(gpDisplay,gWindow,"Final Project Window");
	
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);
	
	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	//variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};
	
	//code
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));
	
	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen ? 0 : 1;
	
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;
	
	XSendEvent(gpDisplay,
	           RootWindow(gpDisplay,gpXVisualInfo->screen),
	           False,
	           StructureNotifyMask,
	           &xev);
}

void initialize(void)
{
	//function prototype
	void resize(int, int);
	
	//Setup The log file
	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "w");
	if (!g_fp_logfile)
		exit(-1);
	fclose(g_fp_logfile);

	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
	fprintf(g_fp_logfile, "log crated in append");
	fclose(g_fp_logfile);



        printf("\n\n Created log file");


	//code
	gGLXContext=glXCreateContext(gpDisplay,gpXVisualInfo,NULL,GL_TRUE);
	
	glXMakeCurrent(gpDisplay,gWindow,gGLXContext);
	
	glClearColor(0.0f,0.0f,0.0f,0.0f);
	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	LoadMeshData();
        calculateModelPosition();
	resize(giWindowWidth,giWindowHeight);
}

void display(void)
{
	//code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(translate_x, translate_y, translate_z);
        //glTranslatef(0.0f, 0.0f, -translateback);
	glRotatef(g_rotate, 0.0f, 1.0f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	for (int i = 0; i != g_face_tri.size(); i++)
	{
		glBegin(GL_TRIANGLES);
		for (int j = 0; j != g_face_tri[i].size(); j++)
		{
			int vi = g_face_tri[i][j] - 1;
			glVertex3f(g_vertices[vi][0], g_vertices[vi][1], g_vertices[vi][2]);
		}
		glEnd();
	}
	
	glXSwapBuffers(gpDisplay,gWindow);
}

void resize(int width,int height)
{
	//code 
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45, ((GLfloat)width / (GLfloat)height), 0.1, 100.0);
}

void uninitialize(void)
{
	GLXContext currentGLXContext;
	currentGLXContext=glXGetCurrentContext();

	if(currentGLXContext!=NULL && currentGLXContext==gGLXContext)
	{
		glXMakeCurrent(gpDisplay,0,0);
	}
	
	if(gGLXContext)
	{
		glXDestroyContext(gpDisplay,gGLXContext);
	}
	
	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}
	
	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}
	
	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;	
	}

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}

	if(g_fp_logfile)
	{
	  fclose(g_fp_logfile);
	}
}

void update(void)
{
	g_rotate = g_rotate + MONKEYHEAD_ANGLE_INCREMENT;
	if (g_rotate >= END_ANGLE_POS)
		g_rotate = START_ANGLE_POS;
}

void LoadMeshData(void)
{
        g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
	fprintf(g_fp_logfile, "\n In load mesh function");
	fclose(g_fp_logfile);

	g_fp_meshfile = fopen("Small car.obj", "r");
	if (!g_fp_meshfile)
	{
		uninitialize();
		return;
	}
        
	char * sep_space = " ";
	char *sep_fslash = "/";
	char *first_token = NULL;
	char *token = NULL;
	char *face_tokens[NR_FACE_TOKENS];
	int nr_tokens;
	char *token_vertex_index = NULL;
	char *token_texture_index = NULL;
	char *token_normal_index = NULL;
	int linenumber = 0;
	while (fgets(line, BUFFER_SIZE, g_fp_meshfile) != NULL)
	{
        /*g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
	fprintf(g_fp_logfile, "\n readingline number");
	fclose(g_fp_logfile);*/
        	linenumber++;
		char buf[10];
		sprintf(buf, "%d", linenumber);
		first_token = strtok(line, sep_space);
		if (strcmp(first_token, "v") == S_EQUAL)
		{
			std::vector<float> vec_point_coord(NR_POINT_COORDS);
			for (int i = 0; i != NR_POINT_COORDS; i++)
			{
				vec_point_coord[i] = atof(strtok(NULL, sep_space));
			}
			g_vertices.push_back(vec_point_coord);
		}
		else if (strcmp(first_token, "vt") == S_EQUAL)
		{
			std::vector<float> vec_texture_coord(NR_TEXTURE_COORDS);
			for (int i = 0; i != NR_TEXTURE_COORDS; i++)
			{
				vec_texture_coord[i] = atof(strtok(NULL, sep_space));
				g_texture.push_back(vec_texture_coord);
			}
		}
		else if (strcmp(first_token, "vn") == S_EQUAL)
		{
			std::vector<float> vec_normal_coord(NR_NORMAL_COORDS);
			for (int i = 0; i != NR_NORMAL_COORDS; i++)
				vec_normal_coord[i] = atof(strtok(NULL, sep_space));
			g_normals.push_back(vec_normal_coord);
		}
		else if (strcmp(first_token, "f") == S_EQUAL)
		{
			std::vector<int> triangle_vertex_indices(3), texture_vertex_indices(3), normal_vertex_indices(3);
			memset((void*)face_tokens, 0, NR_FACE_TOKENS);
			nr_tokens = 0;
			while (token = strtok(NULL, sep_space))
			{
				if (strlen(token) < 3)
					break;
				face_tokens[nr_tokens] = token;
				nr_tokens++;
			}
			for (int i = 0; i != NR_FACE_TOKENS; i++)
			{
				token_vertex_index = strtok(face_tokens[i], sep_fslash);
				token_texture_index = strtok(NULL, sep_fslash);
				token_normal_index = strtok(NULL, sep_fslash);
				triangle_vertex_indices[i] = atoi(token_vertex_index);
				texture_vertex_indices[i] = atoi(token_texture_index);
				normal_vertex_indices[i] = atoi(token_normal_index);
			}
			g_face_tri.push_back(triangle_vertex_indices);
			g_face_texture.push_back(texture_vertex_indices);
			g_face_normals.push_back(normal_vertex_indices);
		}
		memset((void*)line, (int)'\0', BUFFER_SIZE);
	}
	fclose(g_fp_meshfile);
	g_fp_meshfile = NULL;

	g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
	fprintf(g_fp_logfile, "g_vertives:%llu g_texture:%llu g_normals:%llu g_face_tri:%llu\n", g_vertices.size(), g_texture.size(), g_normals.size(), g_face_tri.size());
	fclose(g_fp_logfile);

        g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
	fprintf(g_fp_logfile, "\n done loading mesh");
	fclose(g_fp_logfile);
}


//Functor to calculate minimum from the vector of vector(pointx, pointy, pointz)
// Our vertices list is as follow
// <0, 1, 0>
// <1, 0, 1>
// <1, 1, 1>

template< class T >
struct CustomComparisionOperator {
    CustomComparisionOperator( size_t which_component ) : m_component( which_component ) {}
    bool operator()( const std::vector< T > & left, const std::vector< T > & right ) {
        return left.at( m_component ) < right.at( m_component );
    }
private:
    size_t m_component; //which component of point to compare X , Y or Z i.e 0, 1 or 2 index in vector
};



void calculateModelPosition()
{
  //This calculation is for translating the model so that it is visble to camera
  //Camera is situitated aat origine and looking in z direction 
  // Note: Function need lot of improvement, remove all temporary variables
   
  g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
  fprintf(g_fp_logfile, "\n\ninside calculateModelPosition");
  fclose(g_fp_logfile);

 
  std::vector< std::vector<float> >::iterator itr1 =  std::min_element(g_vertices.begin(), g_vertices.end(), CustomComparisionOperator< float >( 0 ));
  std::vector< std::vector<float> >::iterator itr2 =  std::min_element(g_vertices.begin(), g_vertices.end(), CustomComparisionOperator< float >( 1 ));
  std::vector< std::vector<float> >::iterator itr3 =  std::min_element(g_vertices.begin(), g_vertices.end(), CustomComparisionOperator< float >( 2 ));
  

  std::vector< std::vector<float> >::iterator itr4 =  std::max_element(g_vertices.begin(), g_vertices.end(), CustomComparisionOperator< float >( 0 ));
  std::vector< std::vector<float> >::iterator itr5 =  std::max_element(g_vertices.begin(), g_vertices.end(), CustomComparisionOperator< float >( 1 ));
  std::vector< std::vector<float> >::iterator itr6 =  std::max_element(g_vertices.begin(), g_vertices.end(), CustomComparisionOperator< float >( 2 ));



  float minx = (*itr1).at(0); float miny = (*itr2).at(1); float minz = (*itr3).at(2);
  float maxx = (*itr4).at(0); float maxy = (*itr5).at(1); float maxz = (*itr6).at(2);


  g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
  fprintf(g_fp_logfile, "\n Min is : %f , %f, %f", minx, miny, minz);
  fprintf(g_fp_logfile, "\n Max is : %f , %f, %f", maxx, maxy, maxz);

  fprintf(g_fp_logfile, "\n\nend calculation");
  fclose(g_fp_logfile);



  float midx = (minx + maxx)/2; float midy = (miny + maxy)/2;  float midz = (minz + maxz)/2;
  float distance = sqrt(pow(minx - maxx, 2) + pow(miny - maxy, 2) +pow(minz - maxz, 2));

  g_fp_logfile = fopen("MONKEYHEADLOADER.LOG", "a");
  fprintf(g_fp_logfile, "\n Mid is : %f , %f, %f \ndistance is : %f", midx, midy, midz, distance);
  fclose(g_fp_logfile);

  
  translate_x = -midx;
  translate_y = -midy;
  translate_z = -midz;
  translateback = distance;

}
